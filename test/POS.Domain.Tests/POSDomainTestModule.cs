﻿using POS.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace POS;

[DependsOn(
    typeof(POSEntityFrameworkCoreTestModule)
    )]
public class POSDomainTestModule : AbpModule
{

}
