﻿using System.Threading.Tasks;
using POS.Localization;
using POS.MultiTenancy;
using POS.Permissions;
using Volo.Abp.Identity.Web.Navigation;
using Volo.Abp.SettingManagement.Web.Navigation;
using Volo.Abp.TenantManagement.Web.Navigation;
using Volo.Abp.UI.Navigation;

namespace POS.Web.Menus;

public class POSMenuContributor : IMenuContributor
{
    public async Task ConfigureMenuAsync(MenuConfigurationContext context)
    {
        if (context.Menu.Name == StandardMenus.Main)
        {
            await ConfigureMainMenuAsync(context);
        }
    }

    private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
    {
        var administration = context.Menu.GetAdministration();
        var l = context.GetLocalizer<POSResource>();

        context.Menu.Items.Insert(
            0,
            new ApplicationMenuItem(
                POSMenus.Home,
                l["Menu:Home"],
                "~/",
                icon: "fas fa-home",
                order: 0
            )
        );
        //Cashboxes
        context.Menu.Items.Insert(
            1,
            new ApplicationMenuItem(
                POSMenus.Cashboxes,
                l["Menu:Cashboxes"],
                "/Cashboxes/Index",
                icon: "fa fa-wallet",
                order: 0,
                requiredPermissionName:POSPermissions.Cashboxes.Default
            )
        );
        //Employees
        context.Menu.Items.Insert(
            2,
            new ApplicationMenuItem(
                POSMenus.Employees,
                l["Menu:Employees"],
                "/Employees/Index",
                icon: "fa fa-users",
                order: 0,
                requiredPermissionName: POSPermissions.Employees.Default
            )
        );
        //Products
        context.Menu.Items.Insert(
            3,
            new ApplicationMenuItem(
                POSMenus.Products,
                l["Menu:Products"],
                "/Products/Index",
                icon: "fa fa-bookmark",
                order: 0,
                requiredPermissionName: POSPermissions.Products.Default
            )
        );
        //Currencies
        context.Menu.Items.Insert(
            4,
            new ApplicationMenuItem(
                POSMenus.Currencies,
                l["Menu:Currencies"],
                "/Currencies/Index",
                icon: "fa fa-money",
                order: 0,
                requiredPermissionName: POSPermissions.Currencies.Default
            )
        );

        context.Menu.AddItem(
            new ApplicationMenuItem(
                "POSMenus.Orders", 
                l["Menu:Orders"],
                requiredPermissionName: POSPermissions.Orders.Default,
                icon: "fa fa-store"
                )
                .AddItem(new ApplicationMenuItem(
                    name: POSMenus.Orders,
                    icon:"fa fa-plus",
                    displayName: l["Menu:AddOrder"],
                    url: "/Orders/AddOrder")
                ).AddItem(new ApplicationMenuItem(
                    name: POSMenus.Orders,
                    icon:"fa fa-cloud",
                    displayName: l["Menu:PreviousOrder"],
                    url: "/Orders/Index")
                 )
        );

        if (MultiTenancyConsts.IsEnabled)
        {
            administration.SetSubItemOrder(TenantManagementMenuNames.GroupName, 1);
        }
        else
        {
            administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
        }

        administration.SetSubItemOrder(IdentityMenuNames.GroupName, 2);
        administration.SetSubItemOrder(SettingManagementMenuNames.GroupName, 3);
    }
}
