﻿namespace POS.Web.Menus;

public class POSMenus
{
    private const string Prefix = "POS";
    public const string Home = Prefix + ".Home";
    public const string Cashboxes = Prefix + ".Cashboxes";
    public const string Employees = Prefix + ".Employees";
    public const string Products = Prefix + ".Products";
    public const string Currencies = Prefix + ".Currencies";
    public const string Orders = Prefix + ".Orders";

    //Add your menu items here...

}
