﻿$(function () {
    var l = abp.localization.getResource("POS");
    abp.ui.setBusy(ProductsDataTable);
    var filter = $("#ProductsSearchForm").serializeFormToObject(true);
    function setFilter() {
        filter.name = $("#NameSrch").val();
    }
    $("#SearchBtn").on('click', function () {

        setFilter();
        ProductsDataTable.ajax.reload();
    });
    $("#ClearSearchBtn").on('click', function () {
        $("#NameSrch").val("");
        $("#CashboxIdSrch").val("00000000-0000-0000-0000-000000000000");
        setFilter();
        ProductsDataTable.ajax.reload();
    });
    var ProductsDataTable = $("#ProductsTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            searching: false,
            paging: true,
            order: [[1, "asc"]],
            scrollx: true,
            ajax: function (data, callback, settings) {
                abp.ajax({
                    url: abp.appPath + 'api/app/product' + abp.utils.buildQueryString([{ name: 'name', value: $("#NameSrch").val() }, { name: 'cashboxId', value: $("#CashboxIdSrch").val() }, { name: 'skipCount', value: filter.skipCount }, { name: 'maxResultCount', value: filter.maxResultCount }]) + '',
                    type: 'GET'
                }).done(function (result) {
                    callback({
                        recordsTotal: result?.totalCount,
                        recordsFiltered: result?.totalCount,
                        data: result?.items
                    });
                }).always(function () {
                    abp.ui.clearBusy(ProductsDataTable);
                });
            },
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.products.product.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    render: (data, type, row, meta) => {
                        return meta.row + 1;
                    }
                },
                {
                    title: l('Name'),
                    data: "name",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('SKU'),
                    data: "sku",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Quantity'),
                    data: "quantity",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Cashbox'),
                    data: "cashbox.name",
                    className: 'dt-body-center dt-head-center'
                }
            ]

        })
    );
});