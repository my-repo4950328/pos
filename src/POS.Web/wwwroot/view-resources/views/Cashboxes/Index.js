﻿$(function () {
    var l = abp.localization.getResource("POS");
    var createModal = new abp.ModalManager(abp.appPath + 'Cashboxes/CreateModal');
    var editModal = new abp.ModalManager(abp.appPath + 'Cashboxes/EditModal');
    $('#AddCashbox').click(function (e) {
        e.preventDefault();
        createModal.open();
    });
    createModal.onResult(function (data) {
        CashboxesDataTable.ajax.reload();
    });
    editModal.onResult(function () {
        CashboxesDataTable.ajax.reload();
    });
    abp.ui.setBusy(CashboxesDataTable);
    var inputParam = function () {
        return {
            name: ''
        }
    }
    var filter = $("#CashboxesSearchForm").serializeFormToObject(true);
    function setFilter() {
        filter.name = $("#NameSrch").val();
    }
    $("#SearchBtn").on('click', function () {

        setFilter();
        CashboxesDataTable.ajax.reload();
    });
    $("#ClearSearchBtn").on('click', function () {
        $("#NameSrch").val("");
        setFilter();
        CashboxesDataTable.ajax.reload();
    });
    var responseCallback = function (result) {

        abp.ui.clearBusy(CashboxesDataTable);

        return {
            recordsTotal: result.totalCount,
            recordsFiltered: result.totalCount,
            data: result.items
        };
    };
    var CashboxesDataTable = $("#CashboxesTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            searching: false,
            paging: true,
            order: [[1, "asc"]],
            scrollx: true,
            ajax: function (data, callback, settings) {
                abp.ajax({
                    url: abp.appPath + 'api/app/cashbox' + abp.utils.buildQueryString([{ name: 'name', value: $("#NameSrch").val() }, { name: 'skipCount', value: filter.skipCount }, { name: 'maxResultCount', value: filter.maxResultCount }]) + '',
                    type: 'GET'
                }).done(function (result) {
                    callback({
                        recordsTotal: result?.totalCount,
                        recordsFiltered: result?.totalCount,
                        data: result?.items
                    });
                }).always(function () {
                    abp.ui.clearBusy(CashboxesDataTable);
                });
            },
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.cashboxs.cashbox.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    render: (data, type, row, meta) => {
                        return meta.row+1;
                    }
                },
                {
                    title: l('Name'),
                    data: "name"
                },
                {
                    title: l('Balance'),
                    data: "mainBalance"
                },
                {
                    title: 'Status',
                    render: (data, type, row, meta) => {
                        if (row.status == 1)
                            return `<span class="fa fa-check text-success"></span>`
                        else
                            return `<span class="fa fa-close text-danger"></span>`
                    }
                },
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                visible:
                                    abp.auth.isGranted('POS.Cashboxes.Edit'),
                                action: function (data) {
                                    editModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("Delete"),
                                visible:
                                    abp.auth.isGranted('POS.Cashboxes.Delete'),
                                confirmMessage: function (data) {
                                    return l('CashboxDeltetionConfirmationMessage');
                                },
                                action: function (data) {
                                    pOS.cashboxes.cashbox
                                        .delete(data.record.id)
                                        .then(function (data) {
                                            abp.notify.info(
                                                l('SuccessfullyDeleted')
                                            );
                                            CashboxesDataTable.ajax.reload();
                                        });
                                }
                            }

                        ]
                    }
                }
            ]

        })
    );
});