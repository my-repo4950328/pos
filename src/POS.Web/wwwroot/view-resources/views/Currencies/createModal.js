﻿$(function () {
    $('#Currency_IsMain').trigger('change');
    $('#Currency_IsMain').on('change', function (e) {
        e.preventDefault();
        if ($("#Currency_IsMain").is(':checked')) {
            $("#ExchangeRateDiv").slideUp(500);
            $("#Currency_Amount").val(1).trigger('change');
        }
            
        else
            $("#ExchangeRateDiv").slideDown(500);
      
    })
})