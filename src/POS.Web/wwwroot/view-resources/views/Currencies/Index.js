﻿$(function () {
    var l = abp.localization.getResource("POS");
    var createModal = new abp.ModalManager(abp.appPath + 'Currencies/CreateModal');
    var editModal = new abp.ModalManager(abp.appPath + 'Currencies/EditModal');
    $('#AddCurrency').click(function (e) {
        e.preventDefault();
        createModal.open();
    });
    createModal.onResult(function (data) {
        CurrenciesDataTable.ajax.reload();
    });
    editModal.onResult(function () {
        CurrenciesDataTable.ajax.reload();
    });
    abp.ui.setBusy(CurrenciesDataTable);
    var filter = $("#CurrenciesSearchForm").serializeFormToObject(true);
    function setFilter() {
        filter.name = $("#NameSrch").val();
    }
    $("#SearchBtn").on('click', function () {

        setFilter();
        CurrenciesDataTable.ajax.reload();
    });
    $("#ClearSearchBtn").on('click', function () {
        $("#NameSrch").val("");
        setFilter();
        CurrenciesDataTable.ajax.reload();
    });
    var CurrenciesDataTable = $("#CurrenciesTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            searching: false,
            paging: true,
            order: [[1, "asc"]],
            scrollx: true,
            ajax: function (data, callback, settings) {
                abp.ajax({
                    url: abp.appPath + 'api/app/currencies' + abp.utils.buildQueryString([{ name: 'name', value: $("#NameSrch").val() }, { name: 'skipCount', value: filter.skipCount }, { name: 'maxResultCount', value: filter.maxResultCount }]) + '',
                    type: 'GET'
                }).done(function (result) {
                    callback({
                        recordsTotal: result?.totalCount,
                        recordsFiltered: result?.totalCount,
                        data: result?.items
                    });
                }).always(function () {
                    abp.ui.clearBusy(CurrenciesDataTable);
                });
            },
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.currencies.currency.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    render: (data, type, row, meta) => {
                        return meta.row + 1;
                    }
                },
                {
                    title: l('Name'),
                    data: "name",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Code'),
                    data: "code",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('ExchangeRate'),
                    data: "exchangeRate",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('IsMain'),
                    className: 'dt-body-center dt-head-center',
                    data: "isMain",
                    render: (data, type, row, meta) => {
                        if (row.isMain)
                            return `<span class="fa fa-check text-success"></span>`
                        else
                            return `<span class="fa fa-close text-danger"></span>`
                    }
                },
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                visible:
                                    abp.auth.isGranted('POS.Currencies.Edit'),
                                action: function (data) {
                                    editModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("Manage"),
                                visible:
                                    abp.auth.isGranted('POS.Currencies.Manage'),
                                action: function (data) {
                                    window.location.href = "/Currencies/ExchangeRates?CurrencyId=" + data.record.id;
                                }
                            },
                            {
                                text: l("Delete"),
                                visible:
                                    abp.auth.isGranted('POS.Currencies.Delete'),
                                confirmMessage: function (data) {
                                    return l('CurrencyDeltetionConfirmationMessage');
                                },
                                action: function (data) {
                                    pOS.currencies.currencies
                                        .delete(data.record.id)
                                        .then(function (data) {
                                            abp.notify.info(
                                                l('SuccessfullyDeleted')
                                            );
                                            CurrenciesDataTable.ajax.reload();
                                        });
                                }
                            }

                        ]
                    }
                }
            ]

        })
    );
});