﻿$(function () {
    $("#DateSrch").datepicker();
    var l = abp.localization.getResource("POS");
    var createModal = new abp.ModalManager(abp.appPath + 'Currencies/CreateERModal');
    var editModal = new abp.ModalManager(abp.appPath + 'ExchangeRates/EditModal');
    $('#AddExchangeRate').click(function (e) {
        var id = $("#CurrencyId").val();
        e.preventDefault();
        createModal.open({ currencyId: id });
    });
    createModal.onResult(function (data) {
        ExchangeRatesDataTable.ajax.reload();
        
    });
    editModal.onResult(function () {
        ExchangeRatesDataTable.ajax.reload();
    });
    abp.ui.setBusy(ExchangeRatesDataTable);
    var filter = $("#ExchangeRatesSearchForm").serializeFormToObject(true);
    function setFilter() {
        filter.name = $("#NameSrch").val();
    }
    $("#SearchBtn").on('click', function () {

        setFilter();
        ExchangeRatesDataTable.ajax.reload();
    });
    $("#ClearSearchBtn").on('click', function () {
        $("#NameSrch").val("");
        setFilter();
        ExchangeRatesDataTable.ajax.reload();
    });
    var ExchangeRatesDataTable = $("#ExchangeRatesTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            searching: false,
            paging: true,
            order: [[1, "asc"]],
            scrollx: true,
            ajax: function (data, callback, settings) {
                abp.ajax({
                    url: abp.appPath + 'api/app/currencies/currency-exchange-rates' + abp.utils.buildQueryString([{ name: 'currencyId', value: $("#CurrencyId").val() }, { name: 'skipCount', value: filter.skipCount }, { name: 'maxResultCount', value: filter.maxResultCount }]) + '',
                    type: 'GET'
                }).done(function (result) {
                    callback({
                        recordsTotal: result?.totalCount,
                        recordsFiltered: result?.totalCount,
                        data: result?.items
                    });
                }).always(function () {
                    abp.ui.clearBusy(ExchangeRatesDataTable);
                });
            },
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.currencies.currency.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    className: 'dt-body-center dt-head-center',
                    render: (data, type, row, meta) => {
                        if ((meta.row + 1) == 1)
                            $("#ExchangeRateValue").html(row.amount);
                        return meta.row + 1;
                    }
                },
                {
                    title: l('Amount'),
                    data: "amount",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Status'),
                    data: "status",
                    className: 'dt-body-center dt-head-center',
                    render: (data, type, row, meta) => {
                        if(row.status==3)
                            return '<i class="fa fa-equals text-primary" style="margin-left:auto!important;margin-right:auto!important"></i>'
                        if (row.status == 1)
                            return '<i class="fa fa-arrow-up text-success" style="margin-left:auto!important;margin-right:auto!important"></i>'
                        if (row.status == 2)
                            return '<i class="fa fa-arrow-down text-danger" style="margin-left:auto!important;margin-right:auto!important"></i>'
                    }
                },
                {
                    title: 'Date',
                    className: 'dt-body-center dt-head-center',
                    render: (data, type, row, meta) => {
                        var formattedDate = new Date(row.date);
                        var d = formattedDate.toLocaleString('en', { month: 'short' });
                        var m = formattedDate.getUTCMonth();
                        m += 1;  // JavaScript months are 0-11
                        var y = formattedDate.getFullYear();
                        return `${m}/${d}/${y} ` + formattedDate.toLocaleTimeString("en");
                    }
                },
            ]

        })
    );
})