﻿$(function () {
    $('input[type="radio"]').trigger('change');
    $('input[type="radio"]').on('change', function (e) {
        e.preventDefault();
        var val = e.currentTarget.value;
        if (val == 2) {
            $('.CashboxDiv').slideUp();
        }
        else if(val==1){
            $('.CashboxDiv').slideDown();
        }
    })
})