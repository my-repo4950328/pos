﻿$(function () {
    var l = abp.localization.getResource("POS");
    var createModal = new abp.ModalManager(abp.appPath + 'Employees/CreateModal');
    var editModal = new abp.ModalManager(abp.appPath + 'Employees/EditModal');
    var resetPasswordModal = new abp.ModalManager(abp.appPath + 'Employees/ResetPasswordModal');
    //var editModal = new abp.ModalManager({
    //    viewUrl: '/Employees/EditModal',
    //    scriptUrl: '/view-resources/views/Employees/editModal.js',
    //    modalClass: 'QuestionInfo'
    //});
    //var createModal = new abp.ModalManager({
    //    viewUrl: '/Employees/CreateModal',
    //    scriptUrl: '/view-resources/views/Employees/creatModal.js',
    //    modalClass: 'QuestionInfo'
    //});
    $('#AddEmployee').click(function (e) {
        e.preventDefault();
        createModal.open();
    });
    createModal.onResult(function (data) {
        EmployeesDataTable.ajax.reload();
    });
    editModal.onResult(function () {
        EmployeesDataTable.ajax.reload();
    });
    abp.ui.setBusy(EmployeesDataTable);
    var inputParam = function () {
        return {
            name: ''
        }
    }
    var filter = $("#EmployeesSearchForm").serializeFormToObject(true);
    function setFilter() {
        filter.name = $("#NameSrch").val();
    }
    $("#SearchBtn").on('click', function () {

        setFilter();
        EmployeesDataTable.ajax.reload();
    });
    $("#ClearSearchBtn").on('click', function () {
        $("#NameSrch").val("");
        setFilter();
        EmployeesDataTable.ajax.reload();
    });
    var responseCallback = function (result) {

        abp.ui.clearBusy(EmployeesDataTable);

        return {
            recordsTotal: result.totalCount,
            recordsFiltered: result.totalCount,
            data: result.items
        };
    };
    var EmployeesDataTable = $("#EmployeesTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            searching: false,
            paging: true,
            order: [[1, "asc"]],
            scrollx: true,
            ajax: function (data, callback, settings) {
                abp.ajax({
                    url: abp.appPath + 'api/app/employee' + abp.utils.buildQueryString([{ name: 'name', value: $("#NameSrch").val() }, { name: 'skipCount', value: filter.skipCount }, { name: 'maxResultCount', value: filter.maxResultCount }]) + '',
                    type: 'GET'
                }).done(function (result) {
                    callback({
                        recordsTotal: result?.totalCount,
                        recordsFiltered: result?.totalCount,
                        data: result?.items
                    });
                }).always(function () {
                    abp.ui.clearBusy(EmployeesDataTable);
                });
            },
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.employes.employe.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    render: (data, type, row, meta) => {
                        return meta.row+1;
                    },
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Name'),
                    data: "name",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('UserName'),
                    data: "userName",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Email'),
                    data: "email",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('PhoneNumber'),
                    data: "phoneNumber",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Cashbox'),
                    data: "cashbox.name",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                visible:
                                    abp.auth.isGranted('POS.Employees.Edit'),
                                action: function (data) {
                                    editModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("ResetPassword"),
                                visible:
                                    abp.auth.isGranted('POS.Employees.Manage'),
                                action: function (data) {
                                    resetPasswordModal.open({ id: data.record.userId });
                                }
                            },
                            {
                                text: l("Delete"),
                                visible:
                                    abp.auth.isGranted('POS.Employees.Delete'),
                                confirmMessage: function (data) {
                                    return l('EmployeDeltetionConfirmationMessage');
                                },
                                action: function (data) {
                                    pOS.employees.employee
                                        .delete(data.record.id)
                                        .then(function (data) {
                                            abp.notify.info(
                                                l('SuccessfullyDeleted')
                                            );
                                            EmployeesDataTable.ajax.reload();
                                        });
                                }
                            }

                        ]
                    },
                    className: 'dt-body-center dt-head-center'
                }
            ]

        })
    );
});