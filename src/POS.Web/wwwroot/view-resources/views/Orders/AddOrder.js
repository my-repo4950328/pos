﻿$(function () {
    function validate() {
        let res = true;
        if (data == null || (data != null && data.length <= 0)) {
            $("#OneProductDanger").show();
            res = false;
        }
        else {
            $("#OneProductDanger").hide();

            let MainTotal = parseFloat($("#MainBalance").html());
            let SecondTotal = parseFloat($("#SecondBalance").html());
            let MainPaid = parseFloat($("#MainPaid").val());
            let SecondPaid = parseFloat($("#SecondPaid").val());
            let ExchangeRate = parseFloat($("#er").html());
            let SecondPaidAsMain = parseFloat(SecondPaid * ExchangeRate);
            let TotalPaid = parseFloat(parseFloat(SecondPaidAsMain) + parseFloat(MainPaid));
            if (TotalPaid < MainTotal) {
                $("#EnoughBalance").show();
                res = false;
            }
            else {
                $("#EnoughBalance").hide();
                if (TotalPaid > MainTotal) {
                    let MainChange = parseFloat($("#MainChange").val());
                    let SecondChange = parseFloat($("#SecondChange").val());
                    let SecondChangeAsMain = parseFloat(SecondChange * ExchangeRate);
                    let remain = parseFloat((TotalPaid - MainTotal).toFixed(2));
                    let totalChange = parseFloat((MainChange + SecondChangeAsMain).toFixed(2));
                    if (remain > totalChange) {
                        $("#EnoughChange").show();
                        res = false;
                    }
                        
                    else
                        $("#EnoughChange").hide();
                }
                else {
                    $("#EnoughBalance").hide();
                    $("#EnoughChange").hide();
                }


            }
        }
           
        return res;
    }
    $("#PayBtn").on('click', function (e) {
        calcMainBalance();
        let valid = validate();
        if (valid) {
            let obj = {
                ClientName: $("#ClientName").val(),
                ClientPhoneNumber: $("#ClientPhoneNumber").val(),
                MainTotal: $("#MainBalance").html(),
                SecTotal: $("#SecondBalance").html(),
                MainPaid: $("#MainPaid").val(),
                SecPaid: $("#SecondPaid").val(),
                MainChange: $("#MainChange").val(),
                SecCahange: $("#SecondChange").val(),
                OrderProducts: data
            };
            abp.ui.setBusy();
            abp.ajax({
                type: 'POST',
                url: abp.appPath + `api/app/orders`,
                data: JSON.stringify(obj),
                async: false
            }).then(function (result) {
                abp.ui.clearBusy();
                abp.message.confirm(titleOrCallback = l('DoYouWantAReceipt') ,"Message" )
                    .then(function (confirmed) {
                        if (confirmed) {
                            console.log('TODO: deleting the role...');
                        }
                    });
            }).error(function (e) {
                abp.ui.clearBusy();
            })
        }
        else
            return;
    })
    function calcChanges() {
        let MainTotal = parseFloat($("#MainBalance").html());
        let SecondTotal = parseFloat($("#SecondBalance").html());
        var MainPaid = parseFloat($("#MainPaid").val());
        var SecondPaid = parseFloat($("#SecondPaid").val());
        let ExchangeRate = parseFloat($("#er").html());
        var SecondPaidAsMain = parseFloat(SecondPaid * ExchangeRate);
        var TotalPaid = parseFloat(parseFloat(SecondPaidAsMain) + parseFloat(MainPaid));
        if (TotalPaid > MainTotal) {
            let MainRemain = parseFloat(parseFloat(TotalPaid) - parseFloat(MainTotal));
            let SecondRemain = parseFloat(parseFloat(MainRemain) / parseFloat(ExchangeRate));
            $("#AutoMainChange").html(MainRemain.toFixed(2));
            $("#AutoSecondChange").html(SecondRemain.toFixed(2));
        }
        else {
            $("#AutoMainChange").html("0.00");
            $("#AutoSecondChange").html("0.00");
        }
    }

    $("#MainPaid").on('change', function (e) {
        calcChanges();

    })
    $("#SecondPaid").on('change', function (e) {
        calcChanges();

    })
    var l = abp.localization.getResource("POS");
    $(document).on('click', ".deleteProduct", function (e) {
        let productId = e.currentTarget.dataset.id;
        let finded = data.findIndex(a => a.productId == productId);
        if (finded > -1) {
            OrdersDataTable.clear();
            data = data.filter(a => a.productId != productId);
            OrdersDataTable.rows.add(data).draw();
        }
        calcMainBalance();
    })
    
    function calcMainBalance() {
        let sum = 0.0;
        if (data != null && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                sum += data[i].totalPrice;
            }
        }
        sum = sum.toFixed(2);
        $("#MainBalance").html(sum);
        let secSum = 0.0;
        let er = parseFloat($("#er").html());
        let res = parseFloat(sum / er);
        res = res.toFixed(2);
        $("#SecondBalance").html(res);
        calcChanges();
    }
    $(document).on('click', ".increaseQuantity", function (e) {
        let productId = e.currentTarget.dataset.id;
        let finded = data.findIndex(a => a.productId == productId);
        if (finded > -1) {
            OrdersDataTable.clear();
            data[finded].quantity += 1;
            data[finded].totalPrice = data[finded].price * data[finded].quantity;
            OrdersDataTable.rows.add(data).draw();
        }
        calcMainBalance();
    })
    $(document).on('click', ".decreaseQuantity", function (e) {
        let productId = e.currentTarget.dataset.id;
        let finded = data.findIndex(a => a.productId == productId);
        if (finded > -1) {
            OrdersDataTable.clear();
            if (data[finded].quantity > 1) {
                data[finded].quantity -= 1;
                data[finded].totalPrice = data[finded].price * data[finded].quantity;
            }

            OrdersDataTable.rows.add(data).draw();
        }
        calcMainBalance();
    })
    $("#AddProductBtn").on('click', function (e) {
        let productId = $("#Sku").val();
        if (productId != null) {
            abp.ajax({
                type: 'GET',
                url: abp.appPath + `api/app/product/${productId}`,
                async:false
            }).then(function (result) {
                OrdersDataTable.clear();
                let finded = data.findIndex(a => a.productId == productId);
                if (finded > -1) {
                    data[finded].quantity += 1;
                    data[finded].totalPrice = data[finded].price * data[finded].quantity;
                }
                else {
                    let obj = {
                        "productId": result.id,
                        "name": result.name,
                        "quantity": 1,
                        "price": result.price,
                        "totalPrice": result.price
                    }
                    data.push(obj);
                }


                OrdersDataTable.rows.add(data).draw();
                calcMainBalance();
            });
        }
        
    })
    var data = [
        //{
        //    "name": "Tiger Nixon",
        //    "quantity": "10",
        //    "price": "3,120",
        //    "totalPrice": "31,20"
        //},
        //{
        //    "name": "Tiger Nixon",
        //    "quantity": "10",
        //    "price": "3,120",
        //    "totalPrice": "31,20"
        //}
    ]
    var OrdersDataTable = $("#OrdersTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: false,
            searching: false,
            paging: false,
            order: [[1, "asc"]],
            scrollx: true,
            data: data,
            /*ajax: abp.libs.datatables.createAjax(cloud.lms.cashboxs.cashbox.getList, inputParam, responseCallback),*/
            columnDefs: [
                {
                    title: '#',
                    className: 'dt-body-center dt-head-center',
                    render: (data, type, row, meta) => {
                        return meta.row + 1;
                    }
                },
                {
                    title: l('Name'),
                    data: "name",
                    className: 'dt-body-center dt-head-center'
                },
                {
                    title: l('Quantity'),
                    className: 'dt-body-center dt-head-center',
                    data: "quantity",
                    render: (data, type, row, meta) => {
                        return `<div class="row"><div class="col-sm-6">
<div class="row">
<div class="col-sm-12"><a class="btn btn-primary increaseQuantity" data-id="${row.productId}" style="padding:3px!important;border-radius:2px!important;width:25px!important;margin-bottom:1px;">+</a></div>
</div>
<div class="row">
<div class="col-sm-12"><a class="btn btn-danger decreaseQuantity" data-id="${row.productId}" style="padding:3px!important;border-radius:2px!important;width:25px!important;">-</a></div>
</div>
</div><div class="col-sm-6 text-start" style="padding-top:15px;">${row.quantity}</div></div>`
                    }
                },
                {
                    title: l('Price'),
                    className: 'dt-body-center dt-head-center',
                    data: "price"
                },
                {
                    title: l('TotalPrice'),
                    className: 'dt-body-center dt-head-center',
                    data: "totalPrice"
                },
                {
                    title: '',
                    className: 'dt-body-center dt-head-center',
                    render: (data, type, row, meta) => {
                        return `<span class="text-danger fa fa-trash deleteProduct" style="cursor:pointer" data-id="${row.id}"></span>`
                    }
                },
            ]

        })
    );
})