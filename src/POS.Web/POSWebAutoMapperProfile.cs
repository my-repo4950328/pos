﻿using AutoMapper;
using POS.Cashboxes.Dto;
using POS.Employees.Dto;
using static POS.Web.Pages.Currencies.CreateModalModel;
using static POS.Web.Pages.Employees.CreateModalModel;
using static POS.Web.Pages.Employees.EditModalModel;

namespace POS.Web;

public class POSWebAutoMapperProfile : Profile
{
    public POSWebAutoMapperProfile()
    {
        CreateMap<AddEmployeeViewModel, AddEmployeeDto>();
        CreateMap<EditEmployeeDto, EditEmployeeViewModel>().ReverseMap();
        CreateMap<EmployeeDto, EditEmployeeViewModel>();
        //CreateMap<AddCurrencyViewModel, AddCashboxDto>();
        //Define your AutoMapper configuration here for the Web project.
    }
}
