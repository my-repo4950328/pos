﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace POS.Web;

[Dependency(ReplaceServices = true)]
public class POSBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "POS";
}
