﻿using POS.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace POS.Web.Pages;

/* Inherit your PageModel classes from this class.
 */
public abstract class POSPageModel : AbpPageModel
{
    protected POSPageModel()
    {
        LocalizationResourceType = typeof(POSResource);
    }
}
