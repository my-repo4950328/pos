using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Cashboxes.Dto;
using POS.Cashboxes;
using POS.Web.Pages.Products.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using System.Linq;

namespace POS.Web.Pages.Products
{
    public class IndexModel : POSPageModel
    {
        public List<SelectListItem> Cashboxes { get; set; }
        [BindProperty]
        public AdvancedSearchViewModel Search { get; set; } = new AdvancedSearchViewModel();
        #region Dependencies
        private readonly ICashboxAppService _cashboxAppService;
        #endregion
        public IndexModel(ICashboxAppService cashboxAppService)
        {
            _cashboxAppService = cashboxAppService;
        }
        public async Task OnGet()
        {
            var cashboxes = await _cashboxAppService.GetListAsync(new GetListCashboxDto());
            Cashboxes = cashboxes.Items.Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList();
        }
    }
    public class AdvancedSearchViewModel
    {
        [SelectItems(nameof(Cashboxes))]
        public Guid CashboxId { get; set; }
        public string Name { get; set; }
    }
}
