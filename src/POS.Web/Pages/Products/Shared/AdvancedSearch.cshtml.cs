using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Cashboxes;
using POS.Cashboxes.Dto;
using POS.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace POS.Web.Pages.Products.Shared
{
    public class AdvancedSearchModel : POSPageModel
    {
        public List<SelectListItem> Cashboxes { get; set; }
        [BindProperty]
        public AdvancedSearchViewModel Search { get; set; }
        #region Dependencies
        private readonly ICashboxAppService _cashboxAppService;
        #endregion
        public AdvancedSearchModel(ICashboxAppService cashboxAppService)
        {
            _cashboxAppService = cashboxAppService;
        }
        public async Task OnGet()
        {
            var cashboxes = await _cashboxAppService.GetListAsync(new GetListCashboxDto());
            Cashboxes = cashboxes.Items.Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList();
        }
    }
    public class AdvancedSearchViewModel
    {
        [SelectItems(nameof(Cashboxes))]
        public Guid CashboxId { get; set; }
        public string Name { get;set; }
    }
}
