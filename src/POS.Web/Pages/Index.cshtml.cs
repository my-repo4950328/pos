﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Users;

namespace POS.Web.Pages;

public class IndexModel : POSPageModel
{
    #region Dependencies
    private readonly ICurrentUser _currentUser;
    #endregion
    public IndexModel(ICurrentUser currentUser)
    {
        _currentUser = currentUser;
    }
    public IActionResult OnGet()
    {
        if (_currentUser == null || (_currentUser != null && !_currentUser.IsAuthenticated))
        {
            return new RedirectToPageResult("/Account/Login");
        }
        return Page();
    }
}
