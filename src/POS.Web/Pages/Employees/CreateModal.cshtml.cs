using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Employees.Dto;
using POS.Employees;
using System.Threading.Tasks;
using POS.Cashboxes.Dto;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using System;
using Volo.Abp.Auditing;
using Volo.Abp.Identity;
using Volo.Abp.Validation;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using POS.Cashboxes;
using System.Linq;

namespace POS.Web.Pages.Employees
{
    public class CreateModalModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty]
        public AddEmployeeViewModel Employe { get; set; }
        public List<SelectListItem> Cashboxes { get; set; }
        public List<SelectListItem> EmployeeTypes { get; set; }
        #endregion

        #region Dependencies
        private readonly IEmployeeAppService _employeAppService;
        private readonly ICashboxAppService _cashboxAppService;
        #endregion

        public CreateModalModel(IEmployeeAppService employeAppService, ICashboxAppService cashboxAppService)
        {
            _employeAppService = employeAppService;
            _cashboxAppService=cashboxAppService;
        }
        public async Task OnGet()
        {
            Employe = new AddEmployeeViewModel() { };
            var cashboxes = await _cashboxAppService.GetListAsync(new GetListCashboxDto());
            Cashboxes = cashboxes.Items.Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList();
            EmployeeTypes = Enum.GetValues(typeof(EmployeeType)).Cast<EmployeeType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString(),
                Selected = v.ToString() == "Accountant" ? true : false
            }).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _employeAppService.CreateAsync(ObjectMapper.Map<AddEmployeeViewModel,AddEmployeeDto>(Employe));
            return NoContent();
        }

        public class AddEmployeeViewModel
        {
            [ForeignKey("UserIdentity")]
            public Guid UserId { get; set; }
            public virtual IdentityUserDto User { get; set; }
            [Required]
            [SelectItems(nameof(Cashboxes))]
            [ForeignKey("Cashbox")]
            public Guid CashboxId { get; set; }
            public virtual CashboxDto Cashbox { get; set; }
            [Required]
            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxUserNameLength))]
            [Display(Name = "UserName")]
            public string UserName { get; set; }

            [Required]
            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxEmailLength))]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxNameLength))]
            [Display(Name = "Name")]
            public string Name { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxSurnameLength))]
            [Display(Name = "Surname")]
            public string Surname { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxPhoneNumberLength))]
            [Display(Name = "PhoneNumber")]
            public string PhoneNumber { get; set; }

            [Required]
            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxPasswordLength))]
            [Display(Name = "Password")]
            [DataType(DataType.Password)]
            [DisableAuditing]
            public string Password { get; set; }

            [Required]
            [SelectItems(nameof(EmployeeTypes))]
            public int TypeId { get; set; }

        }
    }
}
