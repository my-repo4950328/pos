using AutoMapper.Internal.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Cashboxes.Dto;
using POS.Cashboxes;
using System.Threading.Tasks;
using System;
using POS.Employees.Dto;
using POS.Employees;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Volo.Abp.Auditing;
using Volo.Abp.Identity;
using Volo.Abp.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace POS.Web.Pages.Employees
{
    public class EditModalModel : POSPageModel
    {
        #region Dependencies
        private readonly IEmployeeAppService _employeeAppService;
        private readonly ICashboxAppService _cashboxAppService;
        #endregion

        #region BindProperties
        [BindProperty]
        public EditEmployeeViewModel Employee { get; set; }
        [BindProperty(SupportsGet = true)]
        [HiddenInput]
        public Guid id { get; set; }
        public List<SelectListItem> Cashboxes { get; set; }
        public List<SelectListItem> EmployeeTypes { get; set; }
        #endregion

        public EditModalModel(IEmployeeAppService employeeAppService, ICashboxAppService cashboxAppService)
        {
            _employeeAppService = employeeAppService;
            _cashboxAppService = cashboxAppService;
        }

        public async Task OnGet(Guid id)
        {
            Employee = ObjectMapper.Map<EmployeeDto, EditEmployeeViewModel>(await _employeeAppService.GetAsync(id));
            var cashboxes = await _cashboxAppService.GetListAsync(new GetListCashboxDto());
            Cashboxes = cashboxes.Items.Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList();
            EmployeeTypes = Enum.GetValues(typeof(EmployeeType)).Cast<EmployeeType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == Employee.TypeId.ToString() ? true : false
            }).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _employeeAppService.UpdateAsync(id, ObjectMapper.Map<EditEmployeeViewModel, EditEmployeeDto>(Employee));
            return NoContent();
        }
        public class EditEmployeeViewModel
        {
            [ForeignKey("UserIdentity")]
            public Guid UserId { get; set; }
            public virtual IdentityUserDto User { get; set; }
            [Required]
            [SelectItems(nameof(Cashboxes))]
            [ForeignKey("Cashbox")]
            public Guid CashboxId { get; set; }
            public virtual CashboxDto Cashbox { get; set; }
            [Required]
            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxUserNameLength))]
            [Display(Name = "UserName")]
            public string UserName { get; set; }

            [Required]
            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxEmailLength))]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxNameLength))]
            [Display(Name = "Name")]
            public string Name { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxSurnameLength))]
            [Display(Name = "Surname")]
            public string Surname { get; set; }

            [DynamicStringLength(typeof(IdentityUserConsts), nameof(IdentityUserConsts.MaxPhoneNumberLength))]
            [Display(Name = "PhoneNumber")]
            public string PhoneNumber { get; set; }

            [Required]
            [SelectItems(nameof(EmployeeTypes))]
            public int TypeId { get; set; }

        }
    }
}
