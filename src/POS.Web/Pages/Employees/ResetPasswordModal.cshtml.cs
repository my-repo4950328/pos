using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Employees;
using POS.Employees.Dto;
using System;
using System.Threading.Tasks;
using Volo.Abp.Account;
using static POS.Permissions.POSPermissions;
using static POS.Web.Pages.Employees.CreateModalModel;

namespace POS.Web.Pages.Employees
{
    public class ResetPasswordModalModel : POSPageModel
    {
        #region Dependencies
        private readonly IEmployeeAppService _employeAppService;
        #endregion
        #region Bind Properties
        [BindProperty(SupportsGet =true)]
        [HiddenInput]
        public Guid id { get; set; }
        [BindProperty]
        public ResetPasswordDto resetPasswordDto { get; set; }
        #endregion

        public ResetPasswordModalModel(IEmployeeAppService employeAppService)
        {
            _employeAppService=employeAppService;
        }
        public async Task OnGet(Guid id)
        {
            resetPasswordDto = new ResetPasswordDto() { UserId=id};
        }

        public async Task<IActionResult> OnPostAsync()
        {
            resetPasswordDto.ResetToken = "sss";
            await _employeAppService.ResetPassword(resetPasswordDto);
            return NoContent();
        }
    }
}
