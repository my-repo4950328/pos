using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using static Volo.Abp.Identity.IdentityPermissions;

namespace POS.Web.Pages.Currencies
{
    [Authorize(Roles = "admin,Manager")]
    public class IndexModel : POSPageModel
    {
        public void OnGet()
        {
        }
    }
}
