using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static Volo.Abp.Identity.IdentityPermissions;
using POS.Currencies.Dto;
using POS.Currencies;

namespace POS.Web.Pages.Currencies
{
    [Authorize(Roles = "admin,Manager")]
    public class CreateModalModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty]
        public AddCurrencyDto Currency { get; set; }
        #endregion

        #region Dependencies
        private readonly ICurrenciesAppService _currenciesAppService;
        #endregion

        public CreateModalModel(ICurrenciesAppService currenciesAppService)
        {
            _currenciesAppService = currenciesAppService;
        }
        public void OnGet()
        {
            Currency = new AddCurrencyDto() { };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _currenciesAppService.CreateAsync(Currency);
            return NoContent();
        }

    }
}
