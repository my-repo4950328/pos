using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Currencies.Dto;
using POS.Currencies;
using System.Threading.Tasks;
using System;
using POS.Cashboxes.Dto;
using POS.Cashboxes;

namespace POS.Web.Pages.Currencies
{
    public class EditModalModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty(SupportsGet = true)]
        [HiddenInput]
        public Guid id { get; set; }
        [BindProperty]
        public EditCurrencyDto Currency { get; set; }
        #endregion

        #region Dependencies
        private readonly ICurrenciesAppService _currenciesAppService;
        #endregion

        public EditModalModel(ICurrenciesAppService currenciesAppService)
        {
            _currenciesAppService = currenciesAppService;
        }
        public async Task OnGet(Guid id)
        {
            Currency = ObjectMapper.Map<CurrencyDto, EditCurrencyDto>(await _currenciesAppService.GetAsync(id));
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _currenciesAppService.UpdateAsync(id,Currency);
            return NoContent();
        }

    }
}
