using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Currencies;
using POS.Currencies.Dto;
using System;
using System.Threading.Tasks;

namespace POS.Web.Pages.Currencies
{
    public class ExchangeRatesModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty(SupportsGet = true)]
        [HiddenInput]
        public Guid CurrencyId { get; set; }
        [BindProperty]
        public CurrencyDto Currency { get; set; } = new CurrencyDto();
        #endregion
        #region Dependencies
        public ICurrenciesAppService _currenciesAppService { get; set; }
        #endregion
        public ExchangeRatesModel(ICurrenciesAppService currenciesAppService)
        {
            _currenciesAppService=currenciesAppService;
        }
        public async Task OnGet(Guid CurrencyId)
        {
            Currency = await _currenciesAppService.GetAsync(CurrencyId);
        }
    }
}
