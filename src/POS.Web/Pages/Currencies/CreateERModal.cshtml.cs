using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Currencies.Dto;
using POS.Currencies;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static Volo.Abp.Identity.IdentityPermissions;
using System;

namespace POS.Web.Pages.Currencies
{
    [Authorize(Roles = "admin,Manager")]
    public class CreateERModalModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty]
        public AddExchangeRateDto ExchangeRate { get; set; }
        [BindProperty(SupportsGet = true)]
        [HiddenInput]
        public Guid CurrencyId { get; set; }
        #endregion

        #region Dependencies
        private readonly ICurrenciesAppService _currenciesAppService;
        #endregion

        public CreateERModalModel(ICurrenciesAppService currenciesAppService)
        {
            _currenciesAppService = currenciesAppService;
        }
        public void OnGet(Guid CurrencyId)
        {
            ExchangeRate = new AddExchangeRateDto() { CurrencyId=CurrencyId};
        }

        public async Task<IActionResult> OnPostAsync()
        {
            ExchangeRate.CurrencyId= CurrencyId;
            await _currenciesAppService.CreateERAsync(ExchangeRate);
            return NoContent();
        }
    }
}
