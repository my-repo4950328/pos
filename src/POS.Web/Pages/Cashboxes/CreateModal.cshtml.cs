using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Cashboxes;
using POS.Cashboxes.Dto;
using System.Threading.Tasks;
using static Volo.Abp.Identity.IdentityPermissions;

namespace POS.Web.Pages.Cashboxes
{
    [Authorize(Roles = "admin,Manager")]
    public class CreateModalModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty]
        public AddCashboxDto Cashbox { get; set; }
        #endregion

        #region Dependencies
        private readonly ICashboxAppService _cashboxAppService;
        #endregion

        public CreateModalModel(ICashboxAppService cashboxAppService)
        {
            _cashboxAppService = cashboxAppService;
        }
        public void OnGet()
        {
            Cashbox = new AddCashboxDto() { };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _cashboxAppService.CreateAsync(Cashbox);
            return NoContent();
        }
    }
}
