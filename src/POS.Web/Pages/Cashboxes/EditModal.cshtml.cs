using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System;
using POS.Cashboxes;
using POS.Cashboxes.Dto;
using Microsoft.AspNetCore.Authorization;
using static Volo.Abp.Identity.IdentityPermissions;

namespace POS.Web.Pages.Cashboxes
{
    [Authorize(Roles = "admin,Manager")]
    public class EditModalModel : POSPageModel
    {
        #region Dependencies
        private readonly ICashboxAppService _cashboxAppService;
        #endregion

        #region BindProperties
        [BindProperty]
        public EditCashboxDto Cashbox { get; set; }
        [BindProperty(SupportsGet =true)]
        [HiddenInput]
        public Guid id { get; set; }
        #endregion

        public EditModalModel(ICashboxAppService cashboxAppService)
        {
            _cashboxAppService = cashboxAppService;
        }

        public async void OnGet(Guid id)
        {
            Cashbox = ObjectMapper.Map<CashboxDto, EditCashboxDto>(await _cashboxAppService.GetAsync(id));
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _cashboxAppService.UpdateAsync(id, Cashbox);
            return NoContent();
        }
    }
}

