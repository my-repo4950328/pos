using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace POS.Web.Pages.Cashboxes
{
    [Authorize(Roles = "admin,Manager")]
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}
