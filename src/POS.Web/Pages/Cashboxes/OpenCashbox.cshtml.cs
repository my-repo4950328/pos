using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Cashboxes;
using POS.Cashboxes.Dto;
using System;
using System.Threading.Tasks;
using static Volo.Abp.Identity.IdentityPermissions;

namespace POS.Web.Pages.Cashboxes
{
    [Authorize(Roles = "admin,Manager,Accountant")]
    public class OpenCashboxModel : POSPageModel
    {
        #region Bind Properties
        [BindProperty(SupportsGet =true)]
        [HiddenInput]
        public Guid Id { get; set; }
        [BindProperty]
        public CashboxDto Cashbox { get; set; }
        #endregion
        #region Dependencies
        public ICashboxAppService _cashboxAppService { get; set; }
        #endregion
        public OpenCashboxModel(ICashboxAppService cashboxAppService)
        {
            _cashboxAppService = cashboxAppService;
        }
        public async Task OnGet(Guid Id)
        {
            Cashbox = await _cashboxAppService.GetAsync(Id);
        }
    }
}
