using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using POS.Cashboxes.Dto;
using POS.Currencies.Dto;
using POS.Orders.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using POS.Localization;
using System.ComponentModel;
using POS.Cashboxes;

namespace POS.Web.Pages.Orders
{
    public class AddOrderModel : POSPageModel
    {
        #region Bind Properies
        [BindProperty]
        public string Sku { get; set; }
        [BindProperty]
        public AddOrderViewModel Order { get; set; }
        [BindProperty]
        public CashboxDto Cashbox { get; set; }
        #endregion
        #region Dependencies
        public IStringLocalizer<POSResource> _localizer;
        public ICashboxAppService _cashboxAppService;
        #endregion
        public AddOrderModel(IStringLocalizer<POSResource> localizer, ICashboxAppService cashboxAppService)
        {
            _localizer = localizer;
            _cashboxAppService = cashboxAppService;
        }
        public async Task OnGet()
        {
            Order = new AddOrderViewModel();
            Cashbox = await _cashboxAppService.GetUserCashbox();
        }

        public class AddOrderViewModel
        {
            public AddOrderViewModel()
            {
                OrderProducts = new List<AddOrderProductsDto>();
            }
            [DataType("nvarchar(128)")]
            [DisplayName("")]
            public string ClientName { get; set; } = "";
            [DataType("nvarchar(128)")]
            [Phone]
            [DisplayName("")]
            public string ClientPhoneNumber { get; set; } = "";
            [ForeignKey(nameof(Cashbox))]
            public Guid CashboxId { get; set; }
            public virtual CashboxDto Cashbox { get; set; }
            [ForeignKey(nameof(ExchangeRate))]
            public Guid ExchangeRateId { get; set; }
            public virtual ExchangeRateDto ExchangeRate { get; set; }
            [DisplayName("")]
            [DataType("decimal(18,8)")]
            public decimal MainTotal { get; set; }
            [DataType("decimal(18,8)")]
            [DisplayName("")]
            public decimal SecTotal { get; set; }
            [DataType("decimal(18,8)")]
            [DisplayName("")]
            public decimal MainPaid { get; set; }
            [DataType("decimal(18,8)")]
            [DisplayName("")]
            public decimal SecPaid { get; set; }
            [DataType("decimal(18,8)")]
            [DisplayName("")]
            public decimal MainChange { get; set; }
            [DataType("decimal(18,8)")]
            [DisplayName("")]
            public decimal SecCahange { get; set; }
            public ICollection<AddOrderProductsDto> OrderProducts { get; set; }
        }
    }
}
