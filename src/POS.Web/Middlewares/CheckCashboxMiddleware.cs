﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS.Cashboxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Volo.Abp.Identity;

namespace POS.Web.Middlewares
{
    //public class CheckCashboxMiddleware : IMiddleware
    //{
    //    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    //    {
    //        try
    //        {
    //            // Call next middleware
    //            await next(context);
    //        }
    //        catch (Exception ex)
    //        {
    //            await next(context);
    //        }
    //    }
    //}
    public class CheckCashboxMiddleware
    {
        public List<string> Urls  = new List<string>()
        { 
            "/",
            "/Cashboxes",
            "/Orders/AddOrder",
            "/Orders/Index"
        };
        private readonly RequestDelegate _next;

        public CheckCashboxMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        // IMessageWriter is injected into InvokeAsync
        public async Task InvokeAsync(HttpContext httpContext,ICashboxAppService cashboxAppService,IdentityUserManager userManager)
        {
            var user = httpContext.User.Claims.FirstOrDefault(a=>a.Type== "sub");
            if(user!= null && Urls.Contains(httpContext.Request.Path))
            {
                var Id = user.Value;
                var cash = await cashboxAppService.ISCashboxClosed(new Guid(Id));
                if(cash!=null && cash.IsClosed)
                    httpContext.Response.Redirect("/Cashboxes/OpenCashbox?Id="+cash.CashboxId);
            }
                
            await _next(httpContext);
        }
    }
}
