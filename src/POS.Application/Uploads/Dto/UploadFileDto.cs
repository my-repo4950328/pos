﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Uploads.Dto
{
    public class UploadFileDto
    {
        public UploadFileDto(string fileName, string url)
        {
            FileName = fileName;
            URL = url;
        }

        public string FileName { get; set; }
        public string URL { get; set; }

        public static UploadFileDto Create(string fileName, string url) => new UploadFileDto(fileName, url);

    }
}
