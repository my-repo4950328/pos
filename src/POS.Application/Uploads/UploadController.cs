﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS.Files;
using POS.Uploads.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Uploads
{
    public class UploadController : POSAppService
    {
        protected readonly IFileManager _fileManager;
        protected readonly IHttpContextAccessor _accessor;

        public UploadController(IFileManager fileManager, IHttpContextAccessor accessor)
        {
            _fileManager = fileManager;
            _accessor = accessor;
        }

        public async Task<UploadFileDto> SaveFile(IFormFile formFile, [FromQuery] string suffix)
        {
            var fileName = await _fileManager.SaveFormFileAndGetPathAsync(formFile, suffix);
            return UploadFileDto.Create(fileName,
                                        _fileManager.GenerateFileURL(fileName));
        }
        [DisableRequestSizeLimit]
        public async Task<UploadFileDto> SaveMultipleFile(List<IFormFile> formFiles, [FromQuery] string suffix)
        {
            var fileName = await _fileManager.MergePhotosInPdfAndGetPathAsync(formFiles, suffix);
            return UploadFileDto.Create(fileName,
                                        _fileManager.GenerateFileURL(fileName));
        }
    }
}
