﻿using AutoMapper;

namespace POS;

public class POSApplicationAutoMapperProfile : Profile
{
    public POSApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
