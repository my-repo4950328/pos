﻿using AutoMapper;
using POS.Cashboxes.Dto;
using POS.Cashboxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.Employees.Dto;

namespace POS.Employees
{
    public class EmployeeMappingProfile : Profile
    {
        public EmployeeMappingProfile()
        {
            CreateMap<AddEmployeeDto, Employee>();
            CreateMap<EditEmployeeDto, Employee>();
            CreateMap<Employee, EmployeeDto>();
            CreateMap<EmployeeDto, EditEmployeeDto>().ReverseMap() ;
        }
    }
}
