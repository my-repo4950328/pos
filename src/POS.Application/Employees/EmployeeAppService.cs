﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using POS.Cashboxes;
using POS.Cashboxes.Dto;
using POS.Employees.Dto;
using POS.Localization;
using POS.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Account.Settings;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Settings;
using Microsoft.Extensions.Options;
using Volo.Abp.ObjectExtending;
using Volo.Abp.Uow;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Account;

namespace POS.Employees
{
    public class EmployeeAppService : CrudAppService<Employee, EmployeeDto, Guid, GetListEmployeeDto, AddEmployeeDto, EditEmployeeDto>, IEmployeeAppService
    {
        private readonly IStringLocalizer<POSResource> _localizer;
        private readonly IRepository<Cashbox, Guid> _cashboxRepository;
        protected IdentityUserManager UserManager { get; }
        protected IOptions<IdentityOptions> IdentityOptions { get; }
        public EmployeeAppService(IRepository<Employee, Guid> repository, IStringLocalizer<POSResource> localizer,
            IOptions<IdentityOptions> identityOptions, IdentityUserManager userManager) : base(repository)
        {
            GetListPolicyName = POSPermissions.Employees.Default;
            CreatePolicyName = POSPermissions.Employees.Create;
            UpdatePolicyName = POSPermissions.Employees.Edit;
            DeletePolicyName = POSPermissions.Employees.Delete;
            _localizer = localizer;
            IdentityOptions = identityOptions;
            UserManager = userManager;
        }

        protected async override Task<IQueryable<Employee>> CreateFilteredQueryAsync(GetListEmployeeDto input)
        {
            var res = (await Repository.WithDetailsAsync(a=>a.Cashbox)).WhereIf(!String.IsNullOrEmpty(input.Name), s =>
            (s.Name.Trim().ToLower().Contains(input.Name.Trim().ToLower())) ||
            (s.UserName.Trim().ToLower().Contains(input.Name.Trim().ToLower())) ||
            (s.Email.Trim().ToLower().Contains(input.Name.Trim().ToLower()))
            ).AsQueryable<Employee>();
            return res;
        }

        protected override async Task<EmployeeDto> MapToGetOutputDtoAsync(Employee entity)
        {
            var Dto = ObjectMapper.Map<Employee, EmployeeDto>(entity);
            var findedUser = await UserManager.GetByIdAsync(entity.UserId);
            if (findedUser != null && await UserManager.IsInRoleAsync(findedUser, "Accountant"))
                Dto.TypeId = (int)EmployeeType.Accountant;
            else if (findedUser != null && await UserManager.IsInRoleAsync(findedUser, "Manager"))
                Dto.TypeId = (int)EmployeeType.Manager;
            return Dto;
        }
       


        public async override Task<EmployeeDto> CreateAsync(AddEmployeeDto input)
        {
            try
            {
                await CheckSelfRegistrationAsync();

                await IdentityOptions.SetAsync();

                var user = new IdentityUser(GuidGenerator.Create(), input.UserName, input.Email, CurrentTenant.Id);

                input.MapExtraPropertiesTo(user);

                (await UserManager.CreateAsync(user, input.Password)).CheckErrors();

                await UserManager.SetEmailAsync(user, input.Email);
                await UserManager.AddDefaultRolesAsync(user);
                if (input.TypeId == (int)EmployeeType.Accountant)
                    await UserManager.SetRolesAsync(user, new[] { "Accountant" });
                else
                    await UserManager.SetRolesAsync(user, new[] { "Manager" });
                input.UserId = user.Id;
                return await base.CreateAsync(input);
            }
            catch(Exception ex)
            {
                throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);
            }
            
        }
        public override async Task<EmployeeDto> UpdateAsync(Guid id, EditEmployeeDto input)
        {

            var findedEmployee = await Repository.GetAsync(a => a.Id == id);
            if (findedEmployee != null)
            {
                findedEmployee.CashboxId = input.CashboxId;
                var user = await UserManager.GetByIdAsync(findedEmployee.UserId);
                input.MapExtraPropertiesTo(user);
                await UserManager.SetEmailAsync(user, input.Email);
                await UserManager.SetUserNameAsync(user, input.UserName);
                await UserManager.SetPhoneNumberAsync(user, input.PhoneNumber);
                user.Surname = input.Surname;
                user.Name = input.Name;
                await UserManager.UpdateAsync(user);
                if (input.TypeId == (int)EmployeeType.Accountant)
                    await UserManager.SetRolesAsync(user, new[] { "Accountant" });
                else
                    await UserManager.SetRolesAsync(user, new[] { "Manager" });

                findedEmployee.Email = input.Email;
                findedEmployee.UserName = input.UserName;
                findedEmployee.PhoneNumber = input.PhoneNumber;
                findedEmployee.Surname = input.Surname;
                findedEmployee.Name = input.Name;
                await Repository.UpdateAsync(findedEmployee);

                await CurrentUnitOfWork.SaveChangesAsync();
                return ObjectMapper.Map<EditEmployeeDto, EmployeeDto>(input);
            }
            else
                throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);
        }
        protected virtual async Task CheckSelfRegistrationAsync()
        {
            if (!await SettingProvider.IsTrueAsync(AccountSettingNames.IsSelfRegistrationEnabled))
            {
                throw new UserFriendlyException(L["SelfRegistrationDisabledMessage"]);
            }
        }
        public override async Task DeleteAsync(Guid id)
        {
            var findedEmployee = await Repository.GetAsync(id);
            if (findedEmployee != null)
            {
                var findedUser = await UserManager.GetByIdAsync(findedEmployee.UserId);
                if (findedUser != null)
                    await UserManager.DeleteAsync(findedUser);
                await Repository.DeleteAsync(findedEmployee);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            else
                throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);
        }

        public async Task ResetPassword(ResetPasswordDto resetPasswordDto)
        {
            var user = await UserManager.GetByIdAsync(resetPasswordDto.UserId);
            var resetToken = await UserManager.GeneratePasswordResetTokenAsync(user);
            await UserManager.ResetPasswordAsync(user, resetToken,resetPasswordDto.Password);
        }

        public async Task<EmployeeDto> GetEmployeeByUserId(Guid userId)
        {
            var finded = (await Repository.GetQueryableAsync()).FirstOrDefault(a => a.UserId == userId);
            if (finded != null)
                return ObjectMapper.Map<Employee,EmployeeDto>(finded);
            throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);

        }
    }
}
