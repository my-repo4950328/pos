﻿using AutoMapper;
using POS.Cashboxes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Cashboxes
{
    public class CashboxesMappingProfile : Profile
    {
        public CashboxesMappingProfile()
        {
            CreateMap<AddCashboxDto, Cashbox>();
            CreateMap<EditCashboxDto, Cashbox>();
            CreateMap<Cashbox,CashboxDto>();
            CreateMap<CashboxDto, EditCashboxDto>();
        }
    }
}
