﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using POS.Cashboxes.Dto;
using POS.Currencies;
using POS.Employees;
using POS.Localization;
using POS.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Users;
using static Volo.Abp.Identity.Settings.IdentitySettingNames;

namespace POS.Cashboxes
{
    public class CashboxAppService : CrudAppService<Cashbox, CashboxDto, Guid, GetListCashboxDto, AddCashboxDto, EditCashboxDto>, ICashboxAppService
    {
        private readonly IStringLocalizer<POSResource> _localizer;
        private readonly IEmployeeAppService _employeeAppService;
        private readonly IdentityUserManager UserManager;
        private readonly ICurrentUser _currentUser;
        private readonly ICurrenciesAppService _currenciesAppService;
        public CashboxAppService(IRepository<Cashbox, Guid> repository,
            IStringLocalizer<POSResource> localizer,
            IEmployeeAppService employeeAppService,
            IdentityUserManager userManager,
            ICurrentUser currentUser,
            ICurrenciesAppService currenciesAppService) : base(repository)
        {
            GetListPolicyName = POSPermissions.Cashboxes.Default;
            CreatePolicyName = POSPermissions.Cashboxes.Create;
            UpdatePolicyName = POSPermissions.Cashboxes.Edit;
            DeletePolicyName = POSPermissions.Cashboxes.Delete;
            _localizer = localizer;
            _employeeAppService=employeeAppService;
            UserManager=userManager;
            _currentUser = currentUser;
            _currenciesAppService=currenciesAppService;
        }

        protected async override Task<IQueryable<Cashbox>> CreateFilteredQueryAsync(GetListCashboxDto input)
        {
            var res = (await Repository.GetListAsync()).WhereIf(!String.IsNullOrEmpty(input.Name), s =>
            s.Name.Trim().ToLower().Contains(input.Name.Trim().ToLower())).AsQueryable<Cashbox>();
            return res;
        }

        public override Task<CashboxDto> CreateAsync(AddCashboxDto input)
        {
            try
            {
                input.MainBalance = 0;
                input.SecBalance = 0;
                input.Status = CashboxStatus.Close;
                return base.CreateAsync(input);
            }
            catch(Exception ex)
            {
                throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);
            }
        }

        public async Task<OpenCashboxDto> ISCashboxClosed(Guid userId)
        {
            var dto = new OpenCashboxDto() { 
                IsClosed=false,
                CashboxId=Guid.NewGuid()
            };
            var user =await UserManager.GetByIdAsync(userId);
            var IsAccountant = await UserManager.IsInRoleAsync(user, "Accountant");
            if (IsAccountant)
            {
                var findedEmployee = await _employeeAppService.GetEmployeeByUserId(user.Id);
                if(findedEmployee!=null)
                {
                    var findedCasbox = await Repository.GetAsync(findedEmployee.CashboxId);
                    if (findedCasbox != null && findedCasbox.Status == POS.CashboxStatus.Close)
                    {
                        dto.IsClosed = true;
                        dto.CashboxId = findedCasbox.Id;
                    }
                }
            }
            return dto;
        }

        public async Task<CashboxDto> GetUserCashbox()
        {
            CashboxDto dto = new CashboxDto();
            var findedEmployee = await _employeeAppService.GetEmployeeByUserId(_currentUser.Id.Value);
            if(findedEmployee!=null)
            {
                var findedCashbox = await Repository.GetAsync(findedEmployee.CashboxId);
                dto = ObjectMapper.Map<Cashbox, CashboxDto>(findedCashbox);
                var findedCurrencies = await _currenciesAppService.GetListAsync(new Currencies.Dto.GetCurrenciesListDto());
                dto.MainCurrency = findedCurrencies.Items.FirstOrDefault(a=>a.IsMain);
                dto.SecCurrency = findedCurrencies.Items.FirstOrDefault(a => !a.IsMain);
                dto.ExchangeRate = (await _currenciesAppService.GetLastExchangeRate()).ToString();
            }
            return dto;
        }

        protected override Task<CashboxDto> MapToGetOutputDtoAsync(Cashbox entity)
        {
            return base.MapToGetOutputDtoAsync(entity);
        }
    }
}
