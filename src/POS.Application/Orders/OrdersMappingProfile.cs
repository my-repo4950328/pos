﻿using AutoMapper;
using POS.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Orders
{
    public class OrdersMappingProfile : Profile
    {
        public OrdersMappingProfile()
        {
            CreateMap<AddOrderDto, Order>();
            CreateMap<EditOrderDto, Order>();
            CreateMap<Order, OrderDto>();
            CreateMap<AddOrderProductsDto, OrderProducts>();
            CreateMap<OrderProducts, OrderProductsDto>();
        }
    }
}
