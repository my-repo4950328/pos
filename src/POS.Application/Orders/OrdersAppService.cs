﻿using Microsoft.Extensions.Localization;
using POS.Cashboxes;
using POS.Currencies;
using POS.Employees;
using POS.Localization;
using POS.Orders.Dto;
using POS.Permissions;
using POS.Transactions.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Users;

namespace POS.Orders
{
    public class OrdersAppService : CrudAppService<Order, OrderDto, Guid, GetListOrdersDto, AddOrderDto, EditOrderDto>, IOrdersAppService
    {
        private readonly IStringLocalizer<POSResource> _localizer;
        private readonly IEmployeeAppService _employeeAppService;
        private readonly ICurrenciesAppService _currenciesAppService;
        private readonly IdentityUserManager UserManager;
        private readonly IRepository<Cashbox, Guid> _cashboxRepository;
        private readonly ICurrentUser _currentUser;
        public OrdersAppService(IRepository<Order, Guid> repository,
            IStringLocalizer<POSResource> localizer,
            IEmployeeAppService employeeAppService,
            IdentityUserManager userManager,
            ICurrentUser currentUser,
            ICurrenciesAppService currenciesAppService,
            IRepository<Cashbox, Guid> cashboxRepository) : base(repository)
        {
            GetListPolicyName = POSPermissions.Orders.Default;
            CreatePolicyName = POSPermissions.Orders.Create;
            UpdatePolicyName = POSPermissions.Orders.Edit;
            DeletePolicyName = POSPermissions.Orders.Delete;
            _localizer = localizer;
            _employeeAppService = employeeAppService;
            UserManager = userManager;
            _currentUser= currentUser;
            _currenciesAppService = currenciesAppService;
            _cashboxRepository = cashboxRepository;
        }

        public override async Task<OrderDto> CreateAsync(AddOrderDto input)
        {
            try
            {
                string code = "";
                string number = "000001";
                var findedEmployee = await _employeeAppService.GetEmployeeByUserId(_currentUser.Id.Value);
                input.CashboxId = findedEmployee.CashboxId;
                var findedExchangeRate = await _currenciesAppService.GetLastExchangeRateId();
                input.ExchangeRateId = findedExchangeRate;
                var findedCashbox = (await _cashboxRepository.GetQueryableAsync()).First(a=>a.Id==input.CashboxId);
                var lastNumber = (await Repository.GetQueryableAsync()).OrderByDescending(a => a.CreationTime).FirstOrDefault(a => a.CashboxId == input.CashboxId);
                number = lastNumber == null ? $"{1:000000}" : 
                    (lastNumber.OrderNumber.Contains("-")?$"{Convert.ToInt64(lastNumber.OrderNumber.Split('-').Last())+1:000000}" : $"{Convert.ToInt64(lastNumber.OrderNumber)+1:000000}");
                if(findedCashbox!=null)
                    code = findedCashbox.Code;
                var orderNumber = !string.IsNullOrEmpty(code) ? $"{code}-{number}" : $"{number}";
                input.OrderNumber = orderNumber;
                AddTransactionDto addTransactionDto = new AddTransactionDto() { 
                    Amount = input.MainTotal,
                    CashboxId = findedCashbox.Id,
                    CurrencyId = (await _currenciesAppService.GetMainCurrency()).Id,
                    Date= DateTime.Now,
                    EmployeeId = findedEmployee.Id,
                    OrderNumber = orderNumber,
                    Type = TransactionType.Payment
                };
                input.Transaction = addTransactionDto;
                //var findeLastCashboxOrder = 
                return await base.CreateAsync(input);
            }
            catch(Exception e)
            {
                throw new UserFriendlyException(_localizer["SomethingGoesWrong"]);
            }

        }
    }
}