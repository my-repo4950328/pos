﻿using AutoMapper;
using POS.Transactions.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Transactions
{
    public class TransactionsMappingProfile : Profile
    {
        public TransactionsMappingProfile()
        {
            CreateMap<AddTransactionDto, Transaction>();
            CreateMap<EditTransactionDto, Transaction>();
            CreateMap<Transaction,TransactionDto>();
        }
    }
}
