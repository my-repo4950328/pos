﻿using Microsoft.Extensions.Localization;
using POS.Employees.Dto;
using POS.Employees;
using POS.Localization;
using POS.Permissions;
using POS.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace POS.Products
{
    public class ProductAppService : CrudAppService<Product, ProductDto, Guid, GetListProductDto, AddProductDto, EditProductDto>, IProductAppService
    {
        private readonly IStringLocalizer<POSResource> _localizer;
        public ProductAppService(IRepository<Product, Guid> repository, IStringLocalizer<POSResource> localizer) : base(repository)
        {
            GetListPolicyName = POSPermissions.Products.Default;
            CreatePolicyName = POSPermissions.Products.Create;
            UpdatePolicyName = POSPermissions.Products.Edit;
            DeletePolicyName = POSPermissions.Products.Delete;
            _localizer = localizer;
        }

        protected async override Task<IQueryable<Product>> CreateFilteredQueryAsync(GetListProductDto input)
        {
            var res = (await Repository.WithDetailsAsync(a => a.Cashbox)).WhereIf(!String.IsNullOrEmpty(input.Name), s =>
            (s.Name.Trim().ToLower().Contains(input.Name.Trim().ToLower())) || (s.Sku.Trim().ToLower().Contains(input.Name.Trim().ToLower()))
            ).WhereIf(input.CashboxId.HasValue && input.CashboxId!= new Guid("00000000-0000-0000-0000-000000000000"),
            s => (s.CashboxId==input.CashboxId)).AsQueryable<Product>();
            return res;
        }
    }
}
