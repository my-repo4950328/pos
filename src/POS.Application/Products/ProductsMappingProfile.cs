﻿using AutoMapper;
using POS.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Products
{
    public class ProductsMappingProfile : Profile
    {
        public ProductsMappingProfile()
        {
            CreateMap<AddProductDto,Product>();
            CreateMap<EditProductDto,Product>();
            CreateMap<Product,ProductDto>();
        }
    }
}
