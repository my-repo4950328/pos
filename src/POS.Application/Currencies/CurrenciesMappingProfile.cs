﻿using AutoMapper;
using POS.Currencies.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Currencies
{
    public class CurrenciesMappingProfile : Profile
    {
        public CurrenciesMappingProfile()
        {
            CreateMap<AddCurrencyDto, Currency>();
            CreateMap<EditCurrencyDto, Currency>(); 
            CreateMap<Currency,CurrencyDto>();
            CreateMap<AddExchangeRateDto, ExchangeRate>();
            CreateMap<ExchangeRate,ExchangeRateDto>();
            CreateMap<CurrencyDto, EditCurrencyDto>();
        }
    }
}
