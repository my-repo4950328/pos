﻿using Microsoft.Extensions.Localization;
using POS.Cashboxes.Dto;
using POS.Cashboxes;
using POS.Currencies.Dto;
using POS.Localization;
using POS.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using System.Linq.Dynamic.Core;
using System.Security.Authentication;

namespace POS.Currencies
{
    public class CurrenciesAppService : CrudAppService<Currency, CurrencyDto, Guid, GetCurrenciesListDto, AddCurrencyDto, EditCurrencyDto>, ICurrenciesAppService
    {
        private readonly IStringLocalizer<POSResource> _localizer;
        private readonly IRepository<ExchangeRate, Guid> _exchangeRatesRepository;
        public CurrenciesAppService(IRepository<Currency, Guid> repository,
            IStringLocalizer<POSResource> localizer,
            IRepository<ExchangeRate, Guid> exchangeRatesRepository) : base(repository)
        {
            GetListPolicyName = POSPermissions.Currencies.Default;
            CreatePolicyName = POSPermissions.Currencies.Create;
            UpdatePolicyName = POSPermissions.Currencies.Edit;
            DeletePolicyName = POSPermissions.Currencies.Delete;
            _exchangeRatesRepository = exchangeRatesRepository;
            _localizer = localizer;
        }

        public async Task<bool> GetCurrencyByCode(string code)
        {
            var valid = true;
            var finded = await Repository.FirstOrDefaultAsync(a => a.Code.ToLower().Trim() == code.ToLower().Trim());
            if (finded != null)
                valid = false;
            return valid;
        }

        public async Task<bool> GetCurrencyByName(string name)
        {
            var valid = true;
            var finded = await Repository.FirstOrDefaultAsync(a => a.Name.ToLower().Trim() == name.ToLower().Trim());
            if (finded != null)
                valid = false;
            return valid;
        }

        protected async override Task<IQueryable<Currency>> CreateFilteredQueryAsync(GetCurrenciesListDto input)
        {
            var res = (await Repository.GetListAsync()).WhereIf(!String.IsNullOrEmpty(input.Name), s =>
            s.Name.Trim().ToLower().Contains(input.Name.Trim().ToLower())).AsQueryable<Currency>();
            return res;
        }

        public override async Task<CurrencyDto> CreateAsync(AddCurrencyDto input)
        {
            string ErrorMessage = "";
            var findeSameCode = await Repository.FirstOrDefaultAsync(a=>a.Code.ToLower().Trim()==input.Code.ToLower().Trim());
            var findeSameName = await Repository.FirstOrDefaultAsync(a => a.Name.ToLower().Trim() == input.Name.ToLower().Trim());
            if (findeSameCode != null)
                ErrorMessage += _localizer["SameCodeError"];
            if (findeSameName != null)
                ErrorMessage += _localizer["SameNameError"];
            if (!string.IsNullOrEmpty(ErrorMessage))
                throw new UserFriendlyException(ErrorMessage);
            if(input.IsMain)
            {
                input.Amount = 1;
                var allEntities = await Repository.GetListAsync(a=>a.IsDeleted || !a.IsDeleted);
                allEntities.ForEach(a=>a.IsMain= false);
                await Repository.UpdateManyAsync(allEntities);
            }
            if (input.Amount <= 0)
                throw new UserFriendlyException(_localizer["ExchangeRateShouldBeBiggerThan0"]);
            input.ExchangeRates.Add(new AddExchangeRateDto() { Amount = input.Amount, Date = DateTime.Now });

            return await base.CreateAsync(input);
        }

        public override async Task<CurrencyDto> UpdateAsync(Guid id, EditCurrencyDto input)
        {
            string ErrorMessage = "";
            var findeSameCode = await Repository.FirstOrDefaultAsync(a => a.Code.ToLower().Trim() == input.Code.ToLower().Trim() && a.Id!=id);
            var findeSameName = await Repository.FirstOrDefaultAsync(a => a.Name.ToLower().Trim() == input.Name.ToLower().Trim() && a.Id != id);
            if (findeSameCode != null)
                ErrorMessage += _localizer["SameCodeError"];
            if (findeSameName != null)
                ErrorMessage += _localizer["SameNameError"];
            if (!string.IsNullOrEmpty(ErrorMessage))
                throw new UserFriendlyException(ErrorMessage);
            if (input.IsMain)
            {
                input.Amount = 1;
                var allEntities = await Repository.GetListAsync(a => a.IsDeleted || !a.IsDeleted);
                allEntities.ForEach(a => a.IsMain = false);
                await Repository.UpdateManyAsync(allEntities);
            }
            if (input.Amount <= 0)
                throw new UserFriendlyException(_localizer["ExchangeRateShouldBeBiggerThan0"]);
            input.ExchangeRates.Add(new AddExchangeRateDto() { Amount = input.Amount, Date = DateTime.Now });
            return await base.UpdateAsync(id, input);
        }


        protected override async Task<CurrencyDto> MapToGetListOutputDtoAsync(Currency entity)
        {
            var res = await base.MapToGetListOutputDtoAsync(entity);
            var findedLastExchnageRate = (await _exchangeRatesRepository.GetQueryableAsync()).Where(a => a.CurrencyId == entity.Id).OrderBy(a => a.Date).LastOrDefault();
            if (findedLastExchnageRate != null)
                res.ExchangeRate = findedLastExchnageRate.Amount;
            return res;
        }
        protected override async Task<CurrencyDto> MapToGetOutputDtoAsync(Currency entity)
        {
            var res = await base.MapToGetOutputDtoAsync(entity);
            var findedLastExchnageRate = (await _exchangeRatesRepository.GetQueryableAsync()).Where(a => a.CurrencyId == entity.Id).OrderBy(a => a.Date).LastOrDefault();
            if (findedLastExchnageRate != null)
                res.ExchangeRate = findedLastExchnageRate.Amount;
            return res;
        }
        public override Task<CurrencyDto> GetAsync(Guid id)
        {
            return base.GetAsync(id);
        }
        public override Task<PagedResultDto<CurrencyDto>> GetListAsync(GetCurrenciesListDto input)
        {
            return base.GetListAsync(input);
        }
        #region Exchange Rate
        private async Task<IQueryable<ExchangeRate>> CreateERFilteredQueryAsync(GetCurrenciesListDto input)
        {
            var res = (await _exchangeRatesRepository.GetListAsync()).WhereIf(input.Date.HasValue, s =>
            s.Date.Date == input.Date).WhereIf(input.CurrencyId.HasValue,s=>s.CurrencyId==input.CurrencyId).AsQueryable<ExchangeRate>();
            return res;
        }
        private IQueryable<ExchangeRate> ApplyERSorting(IQueryable<ExchangeRate> query, GetCurrenciesListDto input)
        {

            //No sorting
            return query.OrderByDescending(a=>a.Date);
        }
        private IQueryable<ExchangeRate> ApplyERPaging(IQueryable<ExchangeRate> query, GetCurrenciesListDto input)
        {
            //Try to use paging if available
            if (input is IPagedResultRequest pagedInput)
            {
                return query.PageBy(pagedInput);
            }

            //Try to limit query result if available
            if (input is ILimitedResultRequest limitedInput)
            {
                return query.Take(limitedInput.MaxResultCount);
            }

            //No paging
            return query;
        }
        private async Task<ExchangeRateDto> MapERToGetListOutputDtoAsync(ExchangeRate entity)
        {
            int status = (int) ExchangeRateStatus.eq;
            var res = ObjectMapper.Map<ExchangeRate, ExchangeRateDto>(entity);
            var finded = (await _exchangeRatesRepository.GetQueryableAsync())
            .Where(a => a.CurrencyId == entity.CurrencyId && a.Id != entity.Id && a.Date <= entity.Date).OrderByDescending(a => a.Date).FirstOrDefault();
            if(finded!=null)
            {

                if (entity.Amount > finded.Amount)
                    status = (int)ExchangeRateStatus.up;
                else
                    if (entity.Amount < finded.Amount)
                    status = (int)ExchangeRateStatus.down;
                
            }
            res.Status = status;
            return res;
        }
        private async Task<List<ExchangeRateDto>> MapERToGetListOutputDtosAsync(List<ExchangeRate> entities)
        {
            var dtos = new List<ExchangeRateDto>();

            foreach (var entity in entities)
            {
                dtos.Add(await MapERToGetListOutputDtoAsync(entity));
            }

            return dtos;
        }
        public async Task<PagedResultDto<ExchangeRateDto>> GetCurrencyExchangeRates(GetCurrenciesListDto input)
        {
            var query = await CreateERFilteredQueryAsync(input);
            var totalCount = await AsyncExecuter.CountAsync(query);
            query = ApplyERSorting(query, input);
            query = ApplyERPaging(query, input);

            var entities = await AsyncExecuter.ToListAsync(query);
            var entityDtos = await MapERToGetListOutputDtosAsync(entities);

            return new PagedResultDto<ExchangeRateDto>(
                totalCount,
                entityDtos
            );
            //List<ExchangeRateDto> res = new List<ExchangeRateDto>();
            //if(dto!=null && dto.CurrencyId.HasValue)
            //{
            //    res = ObjectMapper.Map<List<ExchangeRate>,List<ExchangeRateDto>>(
            //        await _exchangeRatesRepository.GetListAsync(a => a.CurrencyId == dto.CurrencyId).WhereIf();
            //}
            //else
            //    res = ObjectMapper.Map<List<ExchangeRate>, List<ExchangeRateDto>>(
            //        await _exchangeRatesRepository.GetListAsync());

            //return res;
        }
        public async Task CreateERAsync(AddExchangeRateDto input)
        {
            input.Date = DateTime.Now;
            var added = await _exchangeRatesRepository.InsertAsync(ObjectMapper.Map<AddExchangeRateDto, ExchangeRate>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<decimal> GetLastExchangeRate()
        {
            decimal res = 0;
            var findedSecCur = await Repository.FirstOrDefaultAsync(a => !a.IsMain);
            if(findedSecCur != null) {
                var last = (await _exchangeRatesRepository.GetQueryableAsync()).Where(a => a.CurrencyId == findedSecCur.Id)
                    .OrderBy(a => a.Date).LastOrDefault();
                if (last != null)
                    res = last.Amount;
            }
            return res;
        }

        public async Task<Guid> GetLastExchangeRateId()
        {
            Guid res = Guid.NewGuid();
            var findedSecCur = (await Repository.GetQueryableAsync()).FirstOrDefault(a => !a.IsMain);
            if (findedSecCur != null)
            {
                var last = (await _exchangeRatesRepository.GetQueryableAsync()).Where(a => a.CurrencyId == findedSecCur.Id)
                    .OrderBy(a => a.Date).LastOrDefault();
                if (last != null)
                    res = last.Id;
            }
            return res;
        }

        public async Task<CurrencyDto> GetMainCurrency()
        {
            return ObjectMapper.Map<Currency, CurrencyDto>((await Repository.GetQueryableAsync()).FirstOrDefault(a => a.IsMain));
        }

        public async Task<CurrencyDto> GetSecondCurrency()
        {
            return ObjectMapper.Map<Currency, CurrencyDto>((await Repository.GetQueryableAsync()).FirstOrDefault(a => !a.IsMain));
        }
        #endregion

    }
}
