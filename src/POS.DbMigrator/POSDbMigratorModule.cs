﻿using POS.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace POS.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(POSEntityFrameworkCoreModule),
    typeof(POSApplicationContractsModule)
    )]
public class POSDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
