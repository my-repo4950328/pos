﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POS.Cashboxes.Dto
{
    public class OpenCashboxDto
    {
        public bool IsClosed { get; set; }
        public Guid CashboxId { get; set; }
    }
}
