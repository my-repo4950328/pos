﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Cashboxes.Dto
{
    public class GetListCashboxDto : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}
