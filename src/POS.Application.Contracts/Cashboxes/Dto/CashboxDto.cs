﻿using POS.Currencies.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Cashboxes.Dto
{
    public class CashboxDto : FullAuditedEntityDto<Guid>
    {
        [DataType("nvarchar(120)")]
        public string Name { get; set; }
        public CashboxStatus Status { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal MainBalance { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal SecBalance { get; set; }
        [DisplayName("")]
        public CurrencyDto MainCurrency { get; set; }
        [DisplayName("")]
        public CurrencyDto SecCurrency { get; set; }
        public string ExchangeRate { get; set; }
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
    }
}
