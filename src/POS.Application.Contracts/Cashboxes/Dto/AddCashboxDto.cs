﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POS.Cashboxes.Dto
{
    public class AddCashboxDto
    {
        [DataType("nvarchar(120)")]
        [Required]
        public string Name { get; set; }
        public CashboxStatus Status { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal MainBalance { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal SecBalance { get; set; }
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
    }
}
