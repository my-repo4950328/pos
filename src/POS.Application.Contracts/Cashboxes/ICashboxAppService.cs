﻿using POS.Cashboxes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace POS.Cashboxes
{
    public interface ICashboxAppService : ICrudAppService<CashboxDto,Guid,GetListCashboxDto,AddCashboxDto,EditCashboxDto>
    {
        public Task<OpenCashboxDto> ISCashboxClosed(Guid userId);
        public Task<CashboxDto> GetUserCashbox();
    }
}
