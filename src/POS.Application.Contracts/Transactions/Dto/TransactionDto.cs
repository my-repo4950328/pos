﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using POS.Cashboxes.Dto;
using POS.Employees.Dto;
using POS.Orders.Dto;
using POS.Currencies.Dto;

namespace POS.Transactions.Dto
{
    public class TransactionDto
    {
        [Required]
        [DataType("datetime2(7)")]
        public DateTime Date { get; set; }
        [Required]
        public TransactionType Type { get; set; }
        [ForeignKey(nameof(Cashbox))]
        public Guid? CashboxId { get; set; }
        public virtual CashboxDto Cashbox { get; set; }
        [ForeignKey(nameof(Employee))]
        public Guid EmployeeId { get; set; }
        public virtual EmployeeDto Employee { get; set; }
        [ForeignKey(nameof(Order))]
        public Guid? OrderId { get; set; }
        public virtual OrderDto Order { get; set; }
        [Required]
        public string OrderNumber { get; set; }
        [Required]
        [DataType("decimal(18,8")]
        public decimal Amount { get; set; }
        [Required]
        [ForeignKey(nameof(Currency))]
        public Guid CurrencyId { get; set; }
        public CurrencyDto Currency { get; set; }
    }
}
