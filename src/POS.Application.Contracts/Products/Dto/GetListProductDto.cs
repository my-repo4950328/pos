﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Products.Dto
{
    public class GetListProductDto : PagedAndSortedResultRequestDto
    {
        public string? Name { get; set; }    
        public Guid? CashboxId { get; set; }
    }
}
