﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;
using POS.Cashboxes.Dto;

namespace POS.Products.Dto
{
    public class ProductDto : FullAuditedEntityDto<Guid>
    {
        [DataType("nvarchar(128)")]
        public string Name { get; set; }
        [DataType("nvarchar(128)")]
        public string Sku { get; set; }
        [DataType("decimal(18,8)")]
        public decimal Quantity { get; set; }
        [ForeignKey(nameof(Cashbox))]
        public Guid CashboxId { get; set; }
        public virtual CashboxDto Cashbox { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public decimal Price { get; set; }
    }
}
