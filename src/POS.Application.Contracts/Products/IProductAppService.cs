﻿using POS.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Threading;

namespace POS.Products
{
    public interface IProductAppService : ICrudAppService<ProductDto,Guid,GetListProductDto,AddProductDto,EditProductDto>
    {
        //public Task<List<ProductDto>> GetAllProductsBySku(string sku);
    }
}
