﻿using POS.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace POS.Permissions;

public class POSPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var posGroup = context.AddGroup(POSPermissions.GroupName);
        #region Cashboxes
        var CashboxesPermission = posGroup.AddPermission(
    POSPermissions.Cashboxes.Default, L("Permission:Cashboxes"));

        CashboxesPermission.AddChild(
            POSPermissions.Cashboxes.Create, L("Permission:Cashboxes.Create"));

        CashboxesPermission.AddChild(
            POSPermissions.Cashboxes.Edit, L("Permission:Cashboxes.Edit"));

        CashboxesPermission.AddChild(
            POSPermissions.Cashboxes.Delete, L("Permission:Cashboxes.Delete"));

        CashboxesPermission.AddChild(
    POSPermissions.Cashboxes.Manage, L("Permission:Cashboxes.Manage"));
        #endregion
        #region Employees
        var EmployeesPermission = posGroup.AddPermission(
    POSPermissions.Employees.Default, L("Permission:Employees"));

        EmployeesPermission.AddChild(
            POSPermissions.Employees.Create, L("Permission:Employees.Create"));

        EmployeesPermission.AddChild(
            POSPermissions.Employees.Edit, L("Permission:Employees.Edit"));

        EmployeesPermission.AddChild(
            POSPermissions.Employees.Delete, L("Permission:Employees.Delete"));

        EmployeesPermission.AddChild(
    POSPermissions.Employees.Manage, L("Permission:Employees.Manage"));
        #endregion
        #region Products
        var ProductsPermission = posGroup.AddPermission(
    POSPermissions.Products.Default, L("Permission:Products"));

        ProductsPermission.AddChild(
            POSPermissions.Products.Create, L("Permission:Products.Create"));

        ProductsPermission.AddChild(
            POSPermissions.Products.Edit, L("Permission:Products.Edit"));

        ProductsPermission.AddChild(
            POSPermissions.Products.Delete, L("Permission:Products.Delete"));

        ProductsPermission.AddChild(
    POSPermissions.Products.Manage, L("Permission:Products.Manage"));
        #endregion
        #region Currencies
        var CurrenciesPermission = posGroup.AddPermission(
    POSPermissions.Currencies.Default, L("Permission:Currencies"));

        CurrenciesPermission.AddChild(
            POSPermissions.Currencies.Create, L("Permission:Currencies.Create"));

        CurrenciesPermission.AddChild(
            POSPermissions.Currencies.Edit, L("Permission:Currencies.Edit"));

        CurrenciesPermission.AddChild(
            POSPermissions.Currencies.Delete, L("Permission:Currencies.Delete"));

        CurrenciesPermission.AddChild(
    POSPermissions.Currencies.Manage, L("Permission:Currencies.Manage"));
        #endregion
        #region Currencies
        var OrdersPermission = posGroup.AddPermission(
    POSPermissions.Orders.Default, L("Permission:Orders"));

        OrdersPermission.AddChild(
            POSPermissions.Orders.Create, L("Permission:Orders.Create"));

        OrdersPermission.AddChild(
            POSPermissions.Orders.Edit, L("Permission:Orders.Edit"));

        OrdersPermission.AddChild(
            POSPermissions.Orders.Delete, L("Permission:Orders.Delete"));

        OrdersPermission.AddChild(
    POSPermissions.Orders.Manage, L("Permission:Orders.Manage"));
        #endregion
        //Define your own permissions here. Example:
        //myGroup.AddPermission(POSPermissions.MyPermission1, L("Permission:MyPermission1"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<POSResource>(name);
    }
}
