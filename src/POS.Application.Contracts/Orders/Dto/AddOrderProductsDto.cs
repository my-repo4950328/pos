﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POS.Orders.Dto
{
    public class AddOrderProductsDto
    {
        [Required]
        public Guid OrderId { get; set; }
        [Required]
        public Guid ProductId { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
