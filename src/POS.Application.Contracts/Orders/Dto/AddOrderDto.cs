﻿using POS.Cashboxes.Dto;
using POS.Currencies.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel;
using POS.Transactions.Dto;

namespace POS.Orders.Dto
{
    public class AddOrderDto
    {
        public AddOrderDto()
        {
            OrderProducts = new List<AddOrderProductsDto>();
        }
        [DataType("nvarchar(128)")]
        public string ClientName { get; set; } = "";
        [DataType("nvarchar(128)")]
        public string ClientPhoneNumber { get; set; } = "";
        [ForeignKey(nameof(Cashbox))]
        public Guid CashboxId { get; set; }
        public virtual CashboxDto Cashbox { get; set; }
        [ForeignKey(nameof(ExchangeRate))]
        public Guid ExchangeRateId { get; set; }
        public virtual ExchangeRateDto ExchangeRate { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainTotal { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecTotal { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainPaid { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecPaid { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainChange { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecCahange { get; set; }
        [DataType("nvarcha(128)")]
        public string OrderNumber { get; set; }
        public virtual AddTransactionDto Transaction { get; set; }
        public ICollection<AddOrderProductsDto> OrderProducts { get; set; }
    }
}
