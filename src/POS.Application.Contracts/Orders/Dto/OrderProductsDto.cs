﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Orders.Dto
{
    public class OrderProductsDto : FullAuditedEntityDto<Guid>
    {
        [Required]
        public Guid OrderId { get; set; }
        [Required]
        public Guid ProductId { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
