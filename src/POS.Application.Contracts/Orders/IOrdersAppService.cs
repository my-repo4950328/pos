﻿using POS.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Services;

namespace POS.Orders
{
    public interface IOrdersAppService : ICrudAppService<OrderDto,Guid,GetListOrdersDto,AddOrderDto,EditOrderDto>
    {
    }
}
