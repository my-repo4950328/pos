﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Currencies.Dto
{
    public class GetCurrenciesListDto : PagedAndSortedResultRequestDto
    {
        public Guid? CurrencyId { get; set; }
        public DateTime? Date { get; set; }
        public string Name { get; set; }
    }
}
