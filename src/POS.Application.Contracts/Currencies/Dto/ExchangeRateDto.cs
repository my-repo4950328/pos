﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POS.Currencies.Dto
{
    public class ExchangeRateDto
    {
        public Guid CurrencyId { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public Decimal Amount { get; set; }
        [DataType("datetime2(7)")]
        public DateTime Date { get; set; }
        public int Status { get; set; }
    }
}
