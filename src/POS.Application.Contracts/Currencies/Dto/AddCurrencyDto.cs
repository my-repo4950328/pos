﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POS.Currencies.Dto
{
    //public class AddCurrencyDto : IValidatableObject
    public class AddCurrencyDto 
    {
        public AddCurrencyDto()
        {
            ExchangeRates = new List<AddExchangeRateDto>();
        }
        [Required]
        [DataType("nvarchar(128)")]
        public string Name { get; set; }
        [Required]
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
        public bool IsMain { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        [Range(0.0000000001, double.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        [DefaultValue(1)]
        public Decimal Amount { get; set; } = 1;
        public virtual ICollection<AddExchangeRateDto> ExchangeRates { get; set; }

        //    public IEnumerable<ValidationResult> Validate(
        //ValidationContext validationContext)
        //    {
        //        var currenciesService = validationContext.GetRequiredService<ICurrenciesAppService>();
        //        var findedSameCode = currenciesService.GetCurrencyByCode(Code).Result;
        //        var findedSameName = currenciesService.GetCurrencyByName(Name).Result;
        //        if (findedSameCode)
        //        {
        //            yield return new ValidationResult(
        //                "Other currency of same code already exist!",
        //                new[] {"Code" }
        //            );
        //        }
        //        if (findedSameCode)
        //        {
        //            yield return new ValidationResult(
        //                "Other currency of same name already exist!",
        //                new[] { "Name" }
        //            );
        //        }
        //    }
    }
}
