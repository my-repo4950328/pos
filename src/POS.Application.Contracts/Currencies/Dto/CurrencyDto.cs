﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Currencies.Dto
{
    public class CurrencyDto : FullAuditedEntityDto<Guid>
    {
        [Required]
        [DataType("nvarchar(128)")]
        public string Name { get; set; }
        [Required]
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
        [Required]
        public bool IsMain { get; set; }

        [NotMapped]
        [DataType("decimal(18,8)")]
        public decimal ExchangeRate { get; set; } = 1;
    }
}
