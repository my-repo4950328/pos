﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POS.Currencies.Dto
{
    public class AddExchangeRateDto
    {
        [Required]
        [DataType("decimal(18,8)")]
        [Range(0.0000000001, double.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public Decimal Amount { get; set; }
        [DataType("datetime2(7)")]
        public DateTime Date { get; set; }
        public Guid CurrencyId { get; set; }
    }
}
