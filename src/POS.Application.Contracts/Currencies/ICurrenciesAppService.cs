﻿using POS.Currencies.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace POS.Currencies
{
    public interface ICurrenciesAppService : ICrudAppService<CurrencyDto,Guid,GetCurrenciesListDto,AddCurrencyDto,EditCurrencyDto>
    {
        public Task<bool> GetCurrencyByCode(string code);
        public Task<bool> GetCurrencyByName(string name);
        public Task<PagedResultDto<ExchangeRateDto>> GetCurrencyExchangeRates(GetCurrenciesListDto input);
        public Task CreateERAsync(AddExchangeRateDto input);
        public Task<decimal> GetLastExchangeRate();
        public Task<Guid> GetLastExchangeRateId();
        public Task<CurrencyDto> GetMainCurrency();
        public Task<CurrencyDto> GetSecondCurrency();
    }
}
