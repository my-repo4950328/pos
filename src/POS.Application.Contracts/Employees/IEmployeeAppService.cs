﻿using POS.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Account;
using Volo.Abp.Application.Services;

namespace POS.Employees
{
    public interface IEmployeeAppService : ICrudAppService<EmployeeDto,Guid,GetListEmployeeDto,AddEmployeeDto,EditEmployeeDto>
    {
        public Task ResetPassword(ResetPasswordDto resetPasswordDto);
        public Task<EmployeeDto> GetEmployeeByUserId(Guid userId);
    }
}
