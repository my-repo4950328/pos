﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace POS.Employees.Dto
{
    public class GetListEmployeeDto : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}
