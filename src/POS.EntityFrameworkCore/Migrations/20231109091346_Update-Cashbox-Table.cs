﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POS.Migrations
{
    public partial class UpdateCashboxTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "USDBalance",
                table: "Cashboxes",
                newName: "SecBalance");

            migrationBuilder.RenameColumn(
                name: "LebBalance",
                table: "Cashboxes",
                newName: "MainBalance");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_OrderId",
                table: "OrderProducts",
                column: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderProducts_Orders_OrderId",
                table: "OrderProducts",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderProducts_Orders_OrderId",
                table: "OrderProducts");

            migrationBuilder.DropIndex(
                name: "IX_OrderProducts_OrderId",
                table: "OrderProducts");

            migrationBuilder.RenameColumn(
                name: "SecBalance",
                table: "Cashboxes",
                newName: "USDBalance");

            migrationBuilder.RenameColumn(
                name: "MainBalance",
                table: "Cashboxes",
                newName: "LebBalance");
        }
    }
}
