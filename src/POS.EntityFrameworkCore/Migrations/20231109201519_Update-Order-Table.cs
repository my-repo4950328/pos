﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POS.Migrations
{
    public partial class UpdateOrderTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Total",
                table: "Orders",
                newName: "SecTotal");

            migrationBuilder.AddColumn<decimal>(
                name: "MainChange",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MainPaid",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MainTotal",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SecCahange",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SecPaid",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainChange",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "MainPaid",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "MainTotal",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SecCahange",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SecPaid",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "SecTotal",
                table: "Orders",
                newName: "Total");
        }
    }
}
