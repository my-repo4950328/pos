﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POS.Migrations
{
    public partial class ChangeCashboxBalancetoUSDBalanceandLebBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Balance",
                table: "Cashboxes",
                newName: "USDBalance");

            migrationBuilder.AddColumn<decimal>(
                name: "LebBalance",
                table: "Cashboxes",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LebBalance",
                table: "Cashboxes");

            migrationBuilder.RenameColumn(
                name: "USDBalance",
                table: "Cashboxes",
                newName: "Balance");
        }
    }
}
