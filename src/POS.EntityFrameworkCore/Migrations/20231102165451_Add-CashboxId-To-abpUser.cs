﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POS.Migrations
{
    public partial class AddCashboxIdToabpUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CashboxId",
                table: "AbpUsers",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AbpUsers_CashboxId",
                table: "AbpUsers",
                column: "CashboxId");

            migrationBuilder.AddForeignKey(
                name: "FK_AbpUsers_Cashboxes_CashboxId",
                table: "AbpUsers",
                column: "CashboxId",
                principalTable: "Cashboxes",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AbpUsers_Cashboxes_CashboxId",
                table: "AbpUsers");

            migrationBuilder.DropIndex(
                name: "IX_AbpUsers_CashboxId",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "CashboxId",
                table: "AbpUsers");
        }
    }
}
