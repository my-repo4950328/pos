﻿using POS.Cashboxes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Products
{
    public class Product : FullAuditedEntity<Guid>
    {
        [DataType("nvarchar(128)")]
        public string Name { get; set; }
        [DataType("nvarchar(128)")]
        public string Sku { get; set; }
        [DataType("decimal(18,8)")]
        public decimal Quantity { get; set; }
        [ForeignKey(nameof(Cashbox))]
        public Guid CashboxId { get; set; }
        public virtual Cashbox Cashbox { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public decimal Price { get; set; }
    }
}
