﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Currencies
{
    public class Currency : FullAuditedEntity<Guid>
    {
        public Currency()
        {
            ExchangeRates = new List<ExchangeRate>();
        }
        [Required]
        [DataType("nvarchar(128)")]
        public string Name { get; set; }
        [Required]
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
        [Required]
        public bool IsMain { get; set; }
        public virtual ICollection<ExchangeRate> ExchangeRates { get; set; }
    }
}
