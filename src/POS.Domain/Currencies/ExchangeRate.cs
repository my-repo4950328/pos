﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Currencies
{
    public class ExchangeRate : FullAuditedEntity<Guid>
    {
        [Required]
        [ForeignKey(nameof(Currency))]
        public Guid CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public Decimal Amount { get; set; }
        [DataType("datetime2(7)")]
        public DateTime Date { get; set; }
    }
}
