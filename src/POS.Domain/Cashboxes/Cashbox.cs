﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace POS.Cashboxes
{
    public class Cashbox : FullAuditedEntity<Guid>
    {
        [DataType("nvarchar(120)")]
        public string Name { get; set; }
        public CashboxStatus Status { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal MainBalance { get; set; }
        [DataType("decimal(18, 8)")]
        public decimal SecBalance { get; set; }
        public virtual ICollection<IdentityUser> Users { get; set; }
        [DataType("nvarchar(128)")]
        public string Code { get; set; }
    }
}
