﻿using POS.Cashboxes;
using POS.Currencies;
using POS.Employees;
using POS.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Transactions
{
    public class Transaction : FullAuditedEntity<Guid>
    {
        [Required]
        [DataType("datetime2(7)")]
        public DateTime Date { get; set; }
        [Required]
        public TransactionType Type { get; set; }
        [ForeignKey(nameof(Cashbox))]
        public Guid? CashboxId { get; set; }
        public virtual Cashbox Cashbox { get; set; }
        [ForeignKey(nameof(Employee))]
        public Guid EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        [ForeignKey(nameof(Order))]
        public Guid? OrderId { get; set; }
        public virtual Order? Order { get; set; }    
        [Required]
        public string OrderNumber { get; set; }
        [Required]
        [DataType("decimal(18,8")]
        public decimal Amount { get; set; }
        [Required]
        [ForeignKey(nameof(Currency))]
        public Guid CurrencyId { get; set; }
        public Currency Currency { get; set; }
    }
}
