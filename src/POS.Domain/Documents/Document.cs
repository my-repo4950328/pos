﻿using POS.Orders;
using POS.Transactions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Volo.Abp.Domain.Entities.Auditing;
using static System.Net.Mime.MediaTypeNames;

namespace POS.Documents
{
    public class Document : FullAuditedEntity<Guid>
    {
        [DataType("nvarchar(MAX)")]
        public string FileName { get; set; }
        [DataType("nvarchar(MAX)")]
        public string FilePath { get; set; }
        public string DocumentTypeCode { get; set; }
        public DocumnetType DocumentType { get; set; }
        [ForeignKey(nameof(Transaction))]
        public Guid? TransactionId { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
