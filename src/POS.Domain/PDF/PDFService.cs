﻿using DinkToPdf;
using DinkToPdf.Contracts;
using DotLiquid;
using POS.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace POS.PDF
{
    public class PDFService : DomainService, IPDFService
    {
        private readonly IConverter _pdfConverter;
        private readonly IFileManager _fileManager;

        public PDFService(IConverter pdfConverter, IFileManager fileManager)
        {
            _pdfConverter = pdfConverter;
            _fileManager = fileManager;
        }

        public async Task<string> GenerateCleranceLetterPDFAsync(IToPDFBase dataObject, string selectedTemplateFilePath, string fileName, string FilePath, double Top = 10, double Bottom = 10, double Left = 10, double Right = 10)
        {
            var fileAsString = _fileManager.ReadFileContentAsStringAsync(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API", selectedTemplateFilePath)).Result;

            Template.NamingConvention = new DotLiquid.NamingConventions.CSharpNamingConvention();
            Template templateObject = Template.Parse(fileAsString);
            Hash objectHash = Hash.FromAnonymousObject(dataObject, true);
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4Extra,
                //Margins = new MarginSettings() { Top = 10, Bottom = 10, Left = 10, Right = 10 },
                Margins = new MarginSettings() { Top = Top, Bottom = Bottom, Left = Left, Right = Right },
                DocumentTitle = "Document"
            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = templateObject.Render(objectHash),
                WebSettings = { DefaultEncoding = "utf-8" },
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            byte[] file = _pdfConverter.Convert(pdf);
            var nameNotUsed = await _fileManager.SaveFileAndGetFileNameAsync(file, FilePath, fileName, null);
            return $"API/File/{fileName}";
        }

        public async Task<string> GeneratePDFAsync(IToPDFBase dataObject, string selectedTemplateFilePath, double Top = 10, double Bottom = 10, double Left = 10, double Right = 10)
        {
            var fileAsString = _fileManager.ReadFileContentAsStringAsync(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API", selectedTemplateFilePath)).Result;

            Template.NamingConvention = new DotLiquid.NamingConventions.CSharpNamingConvention();
            Template templateObject = Template.Parse(fileAsString);
            Hash objectHash = Hash.FromAnonymousObject(dataObject, true);
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4Extra,
                Margins = new MarginSettings() { Top = Top, Bottom = Bottom, Left = Left, Right = Right },
                DocumentTitle = "Document",

            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = templateObject.Render(objectHash),
                WebSettings = { DefaultEncoding = "utf-8" },
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            byte[] file = _pdfConverter.Convert(pdf);

            var fileName = !string.IsNullOrEmpty(dataObject.OrderNumber) ? $"{dataObject.OrderNumber}_{Guid.NewGuid()}.pdf" : $"{Guid.NewGuid()}.pdf";
            var filePathToSave = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", fileName);
            var nameNotUsed = await _fileManager.SaveFileAndGetFileNameAsync(file, filePathToSave, fileName, null);
            //try IronToPdf
            //var renderer = new ChromePdfRenderer();
            //var pdfFile = PdfDocument.FromFile($"wwwroot/API/File/{fileName}");
            //pdfFile.ApplyWatermark(
            //    "<h1 style='color:Red!important'>Watermark</h1>", 30, IronPdf.Editing.VerticalAlignment.Bottom, IronPdf.Editing.HorizontalAlignment.Right);
            //pdfFile.SaveAs(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", "tttt.pdf"));
            //End Try IronToPdf
            return $"API/File/{fileName}";
            //var presignedUrl = await _aWSS3Manager.SaveFileTOAWSAndGetFileNameAsync(file, filePathToSave, fileName, null, "");
            //return presignedUrl;
        }

        public async Task<List<KeyValuePair<string, string>>> GetClearanceLetterNameAndPath()
        {
            var fileName = Guid.NewGuid().ToString() + ".pdf";
            var filePathToSave = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", fileName);
            //var nameNotUsed = await _fileManager.SaveFileAndGetFileNameAsync(file, filePathToSave, fileName, null);
            List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string,string>("FileName",fileName),
                new KeyValuePair<string,string>("FilePath",filePathToSave)
            };
            return keyValuePairs;
        }
    }
}
