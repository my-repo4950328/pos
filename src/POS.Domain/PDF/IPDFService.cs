﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;

namespace POS.PDF
{
    public interface IPDFService : IDomainService, ITransientDependency
    {
        Task<string> GeneratePDFAsync(IToPDFBase dataObject, string selectedTemplateFilePath, double Top = 10, double Bottom = 10, double Left = 10, double Right = 10);
        Task<string> GenerateCleranceLetterPDFAsync(IToPDFBase dataObject, string selectedTemplateFilePath, string fileName, string FilePath, double Top = 10, double Bottom = 10, double Left = 10, double Right = 10);
        Task<List<KeyValuePair<string, string>>> GetClearanceLetterNameAndPath();
    }
}
