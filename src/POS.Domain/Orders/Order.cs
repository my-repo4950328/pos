﻿using POS.Cashboxes;
using POS.Currencies;
using POS.Transactions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Orders
{
    public class Order : FullAuditedEntity<Guid>
    {
        public Order()
        {
            OrderProducts = new List<OrderProducts>();
        }
        [DataType("nvarchar(128)")]
        public string ClientName { get; set; }
        [DataType("nvarchar(128)")]
        public string ClientPhoneNumber { get; set; }
        [ForeignKey(nameof(Cashbox))]
        public Guid CashboxId { get; set; }
        public virtual Cashbox Cashbox { get; set; }
        [ForeignKey(nameof(ExchangeRate))]
        public Guid ExchangeRateId { get; set; }
        public virtual ExchangeRate ExchangeRate { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainTotal { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecTotal { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainPaid { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecPaid { get; set; }
        [DataType("decimal(18,8)")]
        public decimal MainChange { get; set; }
        [DataType("decimal(18,8)")]
        public decimal SecCahange { get; set; }
        [DataType("nvarcha(128)")]
        public string OrderNumber { get; set; }
        public ICollection<OrderProducts> OrderProducts { get; set; }
        public virtual Transaction Transaction { get; set; }

    }
}
