﻿using POS.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace POS.Orders
{
    public class OrderProducts : FullAuditedEntity<Guid>
    {
        [Required]
        [ForeignKey(nameof(Order))]
        public Guid OrderId { get; set; }
        [ForeignKey(nameof(Product))]
        [Required]
        public Guid ProductId { get; set; }
        [Required]
        [DataType("decimal(18,8)")]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
