﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace POS.Files
{
    public interface IFileManager : IDomainService
    {
        Task<string> SaveFormFileAndGetPathAsync(IFormFile file, string suffix);
        Task<string> CreateOrUpdateFileWithWriteAndGetPathAsync(string fileName, string content);
        string GenerateFileURL(string fileName, string partOfPath = "");
        string GenerateFileName(string extension, string suffix = "");
        Task<string> ReadFileContentAsStringAsync(string filePath);
        Task<string> SaveFileAndGetFileNameAsync(byte[] fileByte, string path, string fileName, string extension);
        Task<string> ZipFilesAndGetZipFileName(List<string> fileNames);
        Task<string> MergePhotosInPdfAndGetPathAsync(List<IFormFile> formFiles, string suffix);
        Task DeleteFile(string fileName);
    }
}
