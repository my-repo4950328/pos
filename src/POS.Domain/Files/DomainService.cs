﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;

namespace POS.Files
{
    public class FileManager : DomainService, IFileManager, ITransientDependency
    {

        public async Task<string> SaveFormFileAndGetPathAsync(IFormFile file, string suffix)
        {
            var name = Path.GetFileName(file.FileName);
            var extension = Path.GetExtension(name);
            var fileName = GenerateFileName(extension, suffix ?? Path.GetFileNameWithoutExtension(file.FileName));
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", fileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            return fileName;
        }

        public async Task<string> CreateOrUpdateFileWithWriteAndGetPathAsync(string fileName, string content)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\", fileName);
            await File.WriteAllTextAsync(path, string.Empty);
            await File.WriteAllTextAsync(path, content);
            return path;
        }

        public async Task DeleteFile(string fileName)
        {
            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\", fileName));
            await Task.CompletedTask;
        }

        public string GenerateFileURL(string fileName, string partOfPath = "")
        {
            return Path.Combine(Directory.GetCurrentDirectory(), partOfPath, fileName);
        }

        public string GenerateFileName(string extension, string suffix = "")
        {
            var fileName = (string.IsNullOrEmpty(suffix) ? Guid.NewGuid().ToString() : suffix)
                 + "_"
                 + DateTime.Now.ToString("yyyyMMddHHmmss")
                 + extension;
            return fileName;
        }

        public async Task<string> ReadFileContentAsStringAsync(string filePath)
        {
            StreamReader sw = new StreamReader(filePath);
            string fileContent = await sw.ReadToEndAsync();
            sw.Close();
            return fileContent;
        }

        public async Task<string> SaveFileAndGetFileNameAsync(byte[] fileByte, string path, string fileName, string extension)
        {
            await File.WriteAllBytesAsync(path, fileByte);
            return fileName;
        }

        public async Task<string> ZipFilesAndGetZipFileName(List<string> fileNames)
        {
            var zipFileName = GenerateFileName(".rar");
            //ZipFile.CreateFromDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File"),
            //                            zipFileName,
            //                            compressionLevel: CompressionLevel.Fastest,
            //                            includeBaseDirectory: false);

            using var archive = ZipFile.Open(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", zipFileName), ZipArchiveMode.Create);

            foreach (var item in fileNames)
            {
                var entry = archive.CreateEntryFromFile(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", item),
                                                        Path.GetFileName(item),
                                                        CompressionLevel.Optimal);
            }

            return await Task.FromResult(zipFileName);
        }

        public async Task<string> MergePhotosInPdfAndGetPathAsync(List<IFormFile> formFiles, string suffix)
        {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 10f);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                iTextSharp.text.pdf.PdfWriter writer = null;
                writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, memoryStream);
                pdfDoc.Open();
                foreach (var file in formFiles)
                {
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(file.OpenReadStream());
                    //img.SetAbsolutePosition(0, 0);
                    pdfDoc.SetPageSize(img);
                    pdfDoc.NewPage();
                    pdfDoc.Add(img);
                }
                pdfDoc.Close();

                var fileName = GenerateFileName(".pdf", suffix);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", fileName);
                byte[] bytes = memoryStream.ToArray();
                await File.WriteAllBytesAsync(path, bytes);
                memoryStream.Close();
                return fileName;
            }


        }

        //public async Task<string> MergePhotosInPdfAndGetPathAsync(List<IFormFile> formFiles, string suffix)
        //{
        //    List<PdfDocument> docs = new List<PdfDocument>();
        //    Image
        //    foreach (var file in formFiles)
        //    {
        //        var test = file.OpenReadStream();
        //        docs.Add(new PdfDocument(file.OpenReadStream()));
        //    }
        //    docs[0].AppendPage(docs[1]);

        //    for (int i = 0; i < docs[2].Pages.Count; i = i + 2)
        //        docs[0].InsertPage(docs[2], i);

        //    var fileName = GenerateFileName("pdf", suffix);
        //    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\API\\File", fileName);
        //    docs[0].SaveToFile(path);

        //    foreach (var doc in docs)
        //        doc.Close();


        //    return await Task.FromResult(fileName);
        //}
    }
}
