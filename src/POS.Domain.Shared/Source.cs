﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POS
{
    public enum CashboxStatus
    {
        Open=1,
        Close=2
    }

    public enum EmployeeType
    {
        Accountant=1,
        Manager=2
    }

    public enum ExchangeRateStatus
    {
        up=1,
        down=2,
        eq=3
    }

    public enum TransactionType
    {
        Payment =1
    }
    public enum DocumnetType
    {
        Payment = 1
    }
}
